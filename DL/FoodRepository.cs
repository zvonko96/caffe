﻿using DL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
  public  class FoodRepository
    {

        private string connectionString = "Integrated Security=true;Initial Catalog=Kafic;Data Source=(localdb)\\MSSQLLocalDB";

        public List<Food> GetAllFoods()
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Foods";

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                List<Food> ListOfFood = new List<Food>();

                while (dataReader.Read())
                {
                    Food f = new Food();
                    f.IdFood = dataReader.GetInt32(0);
                    f.FoodName = dataReader.GetString(1);
                    f.Quantity = dataReader.GetFloat(2);
                    f.UnitOfMeasure = dataReader.GetString(3);
                    f.Price = dataReader.GetInt32(4);
                    f.Currency = dataReader.GetString(5);
                    f.CategoryFood = dataReader.GetString(6);

                    ListOfFood.Add(f);
                }

                return ListOfFood;
            }
        }

        public int InsertFood(Food f)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "INSERT INTO Foods VALUES ('" + f.FoodName + "', " + f.Quantity + ",'" + f.UnitOfMeasure + "'," + f.Price + ", '"+ f.Currency + "', '" + f.CategoryFood + "')";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int UpdateFood(Food f)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "UPDATE Foods SET FoodName = '" + f.FoodName + "', Quantity = " + f.Quantity + ",UnitOfMeasure = '" + f.UnitOfMeasure + "', Price = "+ f.Price + ", Currency = '"+ f.Currency + "', CategoryFood = '" + f.CategoryFood + "' WHERE  IdFood= " + f.IdFood + " ";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int DeleteFood(Food f)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "DELETE FROM Foods WHERE IdFood = (" + f.IdFood + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
