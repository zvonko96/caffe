﻿using DL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
  public  class OrderRepository
    {
        private string connectionString = "Integrated Security=true;Initial Catalog=Kafic;Data Source=(localdb)\\MSSQLLocalDB";

        public List<Orders> GetAllOrder()
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Orders";

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                List<Orders> ListOfOrders = new List<Orders>();

                while (dataReader.Read())
                {
                    Orders o = new Orders();
                    o.IdOrder = dataReader.GetInt32(0);
                    o.IdUser = dataReader.GetInt32(1);
                    o.Dat = dataReader.GetDateTime(2);

                    ListOfOrders.Add(o);
                }

                return ListOfOrders;
            }
        }

        public int InsertOrder(Orders o)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "INSERT INTO User VALUES ( " + o.IdUser + "," + o.Dat + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int UpdateOrder(Orders o)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "UPDATE Orders SET  IdUser = " + o.IdUser + ",Date = " + o.Dat + " where IdOrder = " + o.IdOrder + " ";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int DeleteOrderr(Orders o)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "DELETE FROM Orders WHERE IdOrder = (" + o.IdOrder + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
