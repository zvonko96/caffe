﻿using DL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
   public class OrderItemRepository
    {

        private string connectionString = "Integrated Security=true;Initial Catalog=Kafic;Data Source=(localdb)\\MSSQLLocalDB";

        public List<OrderItem> GetAllOrderItem()
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM OrderItems";

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                List<OrderItem> ListOfOrderItems = new List<OrderItem>();

                while (dataReader.Read())
                {
                    OrderItem oi = new OrderItem();
                    oi.IdItem = dataReader.GetInt32(0);
                    oi.IdDrink = dataReader.GetInt32(1);
                    oi.IdFood = dataReader.GetInt32(2);
                    oi.IdOrder = dataReader.GetInt32(3);

                    ListOfOrderItems.Add(oi);
                }

                return ListOfOrderItems;
            }
        }

        public int InsertOrderItem(OrderItem oi)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "INSERT INTO OrderItems VALUES (" + oi.IdDrink+ "," + oi.IdFood + "," + oi.IdOrder + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int UpdateOrderItemr(OrderItem oi)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "UPDATE OrderItems SET IdDrink = " + oi.IdDrink + ", IdFood = " + oi.IdFood + ",IdOrder = " + oi.IdOrder + " WHERE IdItem = "+ oi.IdItem + " ";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int DeleteOrderItem(OrderItem oi)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "DELETE FROM OrderItems WHERE OrderItems = (" + oi.IdItem + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
