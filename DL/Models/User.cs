﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Models
{
    public class User
    {
        private int idUser;
        private String name;
        private String surname;
        private String userName;
        private String passwordUserName;
        private String faccebookEmail;
        private String passwordFacebook;
        private String email;
        private String passwordEmail;
        private String userNameInstagram;
        private String passwordInstagram;
        private int numberOfShoes;
        private String favoriteCity;
        private String favoriteFood;
        private bool isAdmin;

        public int IdUser { get => idUser; set => idUser = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string UserName { get => userName; set => userName = value; }
        public string PasswordUserName { get => passwordUserName; set => passwordUserName = value; }
        public string FaccebookEmail { get => faccebookEmail; set => faccebookEmail = value; }
        public string PasswordFacebook { get => passwordFacebook; set => passwordFacebook = value; }
        public string Email { get => email; set => email = value; }
        public string PasswordEmail { get => passwordEmail; set => passwordEmail = value; }
        public string UserNameInstagram { get => userNameInstagram; set => userNameInstagram = value; }
        public string PasswordInstagram { get => passwordInstagram; set => passwordInstagram = value; }
        public int NumberOfShoes { get => numberOfShoes; set => numberOfShoes = value; }
        public string FavoriteCity { get => favoriteCity; set => favoriteCity = value; }
        public string FavoriteFood { get => favoriteFood; set => favoriteFood = value; }
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
    }
}
