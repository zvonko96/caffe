﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Models
{
    public class Drink
    {
        private int idDrink;
        private String drinkName;
        private Double quantity;
        private String unitOfMeasure;
        private int price;
        private String currency;
        private String categoryDrink;

        public int IdDrink { get => idDrink; set => idDrink = value; }
        public string DrinkName { get => drinkName; set => drinkName = value; }
        public Double Quantity { get => quantity; set => quantity = value; }
        public string UnitOfMeasure { get => unitOfMeasure; set => unitOfMeasure = value; }
        public int Price { get => price; set => price = value; }
        public string Currency { get => currency; set => currency = value; }
        public string CategoryDrink { get => categoryDrink; set => categoryDrink = value; }
    }
}
