﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Models
{
  public class OrderItem
    {
        private int idItem;
        private int idDrink;
        private int idFood;
        private int idOrder;

        public int IdItem { get => idItem; set => idItem = value; }
        public int IdDrink { get => idDrink; set => idDrink = value; }
        public int IdFood { get => idFood; set => idFood = value; }
        public int IdOrder { get => idOrder; set => idOrder = value; }
    }
}
