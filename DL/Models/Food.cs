﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Models
{
    public class Food
    {
        private int idFood;
        private String foodName;
        private double quantity;
        private String unitOfMeasure;
        private int price;
        private String currency;
        private String categoryFood;

        public int IdFood { get => idFood; set => idFood = value; }
        public string FoodName { get => foodName; set => foodName = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public string UnitOfMeasure { get => unitOfMeasure; set => unitOfMeasure = value; }
        public int Price { get => price; set => price = value; }
        public string Currency { get => currency; set => currency = value; }
        public string CategoryFood { get => categoryFood; set => categoryFood = value; }
    }
}
