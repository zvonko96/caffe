﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Models
{
   public class Orders
    {
        private int idOrder;
        private int idUser;
        private DateTime dat;

        public int IdOrder { get => idOrder; set => idOrder = value; }
        public int IdUser { get => idUser; set => idUser = value; }
        public DateTime Dat { get => dat; set => dat = value; }
    }
}
