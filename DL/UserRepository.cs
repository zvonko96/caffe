﻿
using DL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
    public class UserRepository
    {
        private string connectionString = "Integrated Security=true;Initial Catalog=Kafic;Data Source=(localdb)\\MSSQLLocalDB";

        public List<User> GetAllUsers()
        { 
        using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                    sqlConnection.Open();

                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandText = "SELECT * FROM User";

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    List<User> ListOfUsers = new List<User>();

                    while (dataReader.Read())
                    {
                    User u = new User();
                        u.IdUser = dataReader.GetInt32(0);
                        u.Name = dataReader.GetString(1);
                        u.Surname = dataReader.GetString(2);
                        u.UserName = dataReader.GetString(3);
                        u.PasswordUserName = dataReader.GetString(4);
                        u.FaccebookEmail = dataReader.GetString(5);
                        u.PasswordFacebook = dataReader.GetString(6);
                        u.Email = dataReader.GetString(7);
                        u.PasswordEmail = dataReader.GetString(8);
                        u.UserNameInstagram = dataReader.GetString(9);
                        u.PasswordInstagram = dataReader.GetString(10);
                        u.NumberOfShoes = dataReader.GetInt32(11);
                        u.FavoriteCity = dataReader.GetString(12);
                        u.FavoriteFood = dataReader.GetString(13);
                        u.IsAdmin = dataReader.GetBoolean(14);

                    ListOfUsers.Add(u);
                    }

                    return ListOfUsers;
             }
        }

        public int InsertUser(User u)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "INSERT INTO User VALUES ('" + u.Name + "', '" + u.Surname + "','" + u.UserName + "','" + u.PasswordUserName + "','" + u.FaccebookEmail + "','" + u.PasswordFacebook + "','" + u.Email + "','" + u.PasswordEmail + "','" + u.UserNameInstagram + "','" + u.PasswordInstagram + "', " + u.NumberOfShoes + ", '" + u.FavoriteCity + "','" + u.FavoriteFood + "','" + u.IsAdmin + "')";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int UpdateUser(User u)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "UPDATE User SET Name = '" + u.Name + "', Surname = '" + u.Surname + "',UserName = '" + u.UserName + "',PasswordUserName = '" + u.PasswordUserName + "',FacebookEmail = '" + u.FaccebookEmail + "',PasswordFacebook = '" + u.PasswordFacebook + "', Email = '" + u.Email + "',PasswordEmail = '" + u.PasswordEmail + "',UserNameInstagram = '" + u.UserNameInstagram + "',PasswordInstagram = '" + u.PasswordInstagram + "', NumberOfShoes = " + u.NumberOfShoes + ", FavoriteCity= '" + u.FavoriteCity + "',FavoriteFood = '" + u.FavoriteFood + "',isAdmin = '" + u.IsAdmin + "' WHERE UserName = '" + u.UserName + "' ";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int DeleteUser(User u)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "DELETE FROM User WHERE UserName = ('" + u.UserName + "')";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int UpdatePassword(User u)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "UPDATE [User] SET PasswordUserName = '" + u.PasswordUserName + "' WHERE UserName = '" + u.UserName + "' ";

                return sqlCommand.ExecuteNonQuery();
            }
        }

    }
}
