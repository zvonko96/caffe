﻿using DL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
  public  class DrinkRepository
    {
        private string connectionString = "Integrated Security=true;Initial Catalog=Kafic;Data Source=(localdb)\\MSSQLLocalDB";

        public List<Drink> GetAllDrinks()
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Drinks";

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                List<Drink> ListOfDrink = new List<Drink>();

                while (dataReader.Read())
                {
                    Drink d = new Drink();
                    d.IdDrink = dataReader.GetInt32(0);
                    d.DrinkName = dataReader.GetString(1);
                    d.Quantity = dataReader.GetDouble(2);
                    d.UnitOfMeasure = dataReader.GetString(3);
                    d.Price = dataReader.GetInt32(4);
                    d.Currency = dataReader.GetString(5);
                    d.CategoryDrink = dataReader.GetString(6);
                    ListOfDrink.Add(d);
                }

                return ListOfDrink;
            }
        }

        public int InsertDrink(Drink d)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "INSERT INTO Drinks VALUES ('" + d.DrinkName + "', " + d.Quantity + ",'" + d.UnitOfMeasure + "'," + d.Price + ", '" + d.Currency + "', '"+ d.CategoryDrink +"')";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int UpdateDrink(Drink d)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "UPDATE Drinks SET DrinkName = '" + d.DrinkName + "', Quantity = " + d.Quantity + ",UnitOfMeasure = '" + d.UnitOfMeasure + "', Price = " + d.Price + ", Currency = '" + d.Currency + "', CategoryDrink = '"+d.CategoryDrink+"' WHERE  IdDrink= " + d.IdDrink + " ";

                return sqlCommand.ExecuteNonQuery();
            }
        }


        public int DeleteDrink(Drink d)
        {
            using (SqlConnection sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = "DELETE FROM Drinks WHERE IdDrink = (" + d.IdDrink + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
