﻿namespace Aplikacija_kafic
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelLoginAdmin = new System.Windows.Forms.Panel();
            this.buttonLoginAdminMinimized = new System.Windows.Forms.Button();
            this.buttonLoginAdminExit = new System.Windows.Forms.Button();
            this.labelLoginAdminTime = new System.Windows.Forms.Label();
            this.timerLoginAdmin = new System.Windows.Forms.Timer(this.components);
            this.textBoxLoginAdminUserName = new System.Windows.Forms.TextBox();
            this.textBoxLoginAdminPassword = new System.Windows.Forms.TextBox();
            this.buttonLoginAdmin = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.buttonFacebookAdmin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonInstagramAdmin = new System.Windows.Forms.Button();
            this.buttonGmailAdmin = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.linkLabelForgotLoginAdmin = new System.Windows.Forms.LinkLabel();
            this.pictureBoxEye = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelLoginAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(410, 105);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(274, 249);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panelLoginAdmin
            // 
            this.panelLoginAdmin.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panelLoginAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelLoginAdmin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLoginAdmin.Controls.Add(this.buttonLoginAdminMinimized);
            this.panelLoginAdmin.Controls.Add(this.buttonLoginAdminExit);
            this.panelLoginAdmin.Controls.Add(this.labelLoginAdminTime);
            this.panelLoginAdmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLoginAdmin.ForeColor = System.Drawing.Color.Black;
            this.panelLoginAdmin.Location = new System.Drawing.Point(0, 0);
            this.panelLoginAdmin.Name = "panelLoginAdmin";
            this.panelLoginAdmin.Size = new System.Drawing.Size(1366, 38);
            this.panelLoginAdmin.TabIndex = 8;
            // 
            // buttonLoginAdminMinimized
            // 
            this.buttonLoginAdminMinimized.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoginAdminMinimized.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLoginAdminMinimized.ForeColor = System.Drawing.Color.White;
            this.buttonLoginAdminMinimized.Location = new System.Drawing.Point(1291, 1);
            this.buttonLoginAdminMinimized.Name = "buttonLoginAdminMinimized";
            this.buttonLoginAdminMinimized.Size = new System.Drawing.Size(36, 34);
            this.buttonLoginAdminMinimized.TabIndex = 8;
            this.buttonLoginAdminMinimized.Text = "_";
            this.buttonLoginAdminMinimized.UseVisualStyleBackColor = true;
            this.buttonLoginAdminMinimized.Click += new System.EventHandler(this.buttonLoginAdminMinimized_Click);
            // 
            // buttonLoginAdminExit
            // 
            this.buttonLoginAdminExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoginAdminExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLoginAdminExit.ForeColor = System.Drawing.Color.White;
            this.buttonLoginAdminExit.Location = new System.Drawing.Point(1328, 1);
            this.buttonLoginAdminExit.Name = "buttonLoginAdminExit";
            this.buttonLoginAdminExit.Size = new System.Drawing.Size(36, 34);
            this.buttonLoginAdminExit.TabIndex = 7;
            this.buttonLoginAdminExit.Text = "X";
            this.buttonLoginAdminExit.UseVisualStyleBackColor = true;
            this.buttonLoginAdminExit.Click += new System.EventHandler(this.buttonLoginAdminExit_Click);
            this.buttonLoginAdminExit.MouseLeave += new System.EventHandler(this.buttonLoginAdminExit_MouseLeave);
            this.buttonLoginAdminExit.MouseHover += new System.EventHandler(this.buttonLoginAdminExit_MouseHover);
            // 
            // labelLoginAdminTime
            // 
            this.labelLoginAdminTime.AutoSize = true;
            this.labelLoginAdminTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoginAdminTime.ForeColor = System.Drawing.Color.White;
            this.labelLoginAdminTime.Location = new System.Drawing.Point(606, 0);
            this.labelLoginAdminTime.Name = "labelLoginAdminTime";
            this.labelLoginAdminTime.Size = new System.Drawing.Size(78, 31);
            this.labelLoginAdminTime.TabIndex = 6;
            this.labelLoginAdminTime.Text = "Time";
            // 
            // timerLoginAdmin
            // 
            this.timerLoginAdmin.Enabled = true;
            this.timerLoginAdmin.Tick += new System.EventHandler(this.timerLoginAdmin_Tick_1);
            // 
            // textBoxLoginAdminUserName
            // 
            this.textBoxLoginAdminUserName.Font = new System.Drawing.Font("Microsoft Uighur", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLoginAdminUserName.Location = new System.Drawing.Point(532, 387);
            this.textBoxLoginAdminUserName.Name = "textBoxLoginAdminUserName";
            this.textBoxLoginAdminUserName.Size = new System.Drawing.Size(308, 37);
            this.textBoxLoginAdminUserName.TabIndex = 9;
            this.textBoxLoginAdminUserName.TextChanged += new System.EventHandler(this.textBoxLoginAdminUserName_TextChanged);
            // 
            // textBoxLoginAdminPassword
            // 
            this.textBoxLoginAdminPassword.Font = new System.Drawing.Font("Microsoft Uighur", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLoginAdminPassword.Location = new System.Drawing.Point(532, 451);
            this.textBoxLoginAdminPassword.Name = "textBoxLoginAdminPassword";
            this.textBoxLoginAdminPassword.Size = new System.Drawing.Size(308, 37);
            this.textBoxLoginAdminPassword.TabIndex = 10;
            this.textBoxLoginAdminPassword.UseSystemPasswordChar = true;
            this.textBoxLoginAdminPassword.TextChanged += new System.EventHandler(this.textBoxLoginAdminPassword_TextChanged);
            // 
            // buttonLoginAdmin
            // 
            this.buttonLoginAdmin.BackColor = System.Drawing.SystemColors.MenuBar;
            this.buttonLoginAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonLoginAdmin.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonLoginAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLoginAdmin.Font = new System.Drawing.Font("Rockwell", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoginAdmin.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonLoginAdmin.Location = new System.Drawing.Point(494, 507);
            this.buttonLoginAdmin.Name = "buttonLoginAdmin";
            this.buttonLoginAdmin.Size = new System.Drawing.Size(346, 36);
            this.buttonLoginAdmin.TabIndex = 11;
            this.buttonLoginAdmin.Text = "Prijavi se";
            this.buttonLoginAdmin.UseVisualStyleBackColor = false;
            this.buttonLoginAdmin.Click += new System.EventHandler(this.buttonLoginAdmin_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(494, 387);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(38, 37);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(494, 451);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(38, 37);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // buttonFacebookAdmin
            // 
            this.buttonFacebookAdmin.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonFacebookAdmin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonFacebookAdmin.BackgroundImage")));
            this.buttonFacebookAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFacebookAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFacebookAdmin.Location = new System.Drawing.Point(574, 690);
            this.buttonFacebookAdmin.Name = "buttonFacebookAdmin";
            this.buttonFacebookAdmin.Size = new System.Drawing.Size(53, 52);
            this.buttonFacebookAdmin.TabIndex = 14;
            this.buttonFacebookAdmin.UseVisualStyleBackColor = true;
            this.buttonFacebookAdmin.Click += new System.EventHandler(this.buttonFacebookAdmin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(563, 559);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Zaboravili ste ";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(599, 664);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 18);
            this.label2.TabIndex = 18;
            this.label2.Text = "Prijavite se preko";
            // 
            // buttonInstagramAdmin
            // 
            this.buttonInstagramAdmin.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonInstagramAdmin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonInstagramAdmin.BackgroundImage")));
            this.buttonInstagramAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonInstagramAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInstagramAdmin.Location = new System.Drawing.Point(633, 690);
            this.buttonInstagramAdmin.Name = "buttonInstagramAdmin";
            this.buttonInstagramAdmin.Size = new System.Drawing.Size(53, 52);
            this.buttonInstagramAdmin.TabIndex = 19;
            this.buttonInstagramAdmin.UseVisualStyleBackColor = true;
            this.buttonInstagramAdmin.Click += new System.EventHandler(this.buttonInstagramAdmin_Click);
            // 
            // buttonGmailAdmin
            // 
            this.buttonGmailAdmin.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonGmailAdmin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonGmailAdmin.BackgroundImage")));
            this.buttonGmailAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGmailAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGmailAdmin.Location = new System.Drawing.Point(693, 690);
            this.buttonGmailAdmin.Name = "buttonGmailAdmin";
            this.buttonGmailAdmin.Size = new System.Drawing.Size(53, 52);
            this.buttonGmailAdmin.TabIndex = 20;
            this.buttonGmailAdmin.UseVisualStyleBackColor = true;
            this.buttonGmailAdmin.Click += new System.EventHandler(this.buttonGmailAdmin_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(535, 369);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "Unesite korisnicko ime!";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(536, 435);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 16);
            this.label4.TabIndex = 22;
            this.label4.Text = "Unesite lozinku!";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(846, 393);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 26);
            this.label5.TabIndex = 23;
            this.label5.Text = "*";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(846, 457);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 26);
            this.label6.TabIndex = 24;
            this.label6.Text = "*";
            // 
            // linkLabelForgotLoginAdmin
            // 
            this.linkLabelForgotLoginAdmin.AutoSize = true;
            this.linkLabelForgotLoginAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.linkLabelForgotLoginAdmin.LinkColor = System.Drawing.Color.Maroon;
            this.linkLabelForgotLoginAdmin.Location = new System.Drawing.Point(676, 559);
            this.linkLabelForgotLoginAdmin.Name = "linkLabelForgotLoginAdmin";
            this.linkLabelForgotLoginAdmin.Size = new System.Drawing.Size(75, 20);
            this.linkLabelForgotLoginAdmin.TabIndex = 25;
            this.linkLabelForgotLoginAdmin.TabStop = true;
            this.linkLabelForgotLoginAdmin.Text = "lozinku?";
            this.linkLabelForgotLoginAdmin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelForgotLoginAdmin_LinkClicked);
            // 
            // pictureBoxEye
            // 
            this.pictureBoxEye.BackColor = System.Drawing.Color.White;
            this.pictureBoxEye.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxEye.Image = global::Aplikacija_kafic.Properties.Resources.okce;
            this.pictureBoxEye.Location = new System.Drawing.Point(797, 451);
            this.pictureBoxEye.Name = "pictureBoxEye";
            this.pictureBoxEye.Size = new System.Drawing.Size(43, 37);
            this.pictureBoxEye.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEye.TabIndex = 44;
            this.pictureBoxEye.TabStop = false;
            this.pictureBoxEye.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(680, 105);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(274, 249);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 45;
            this.pictureBox4.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBoxEye);
            this.Controls.Add(this.linkLabelForgotLoginAdmin);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonGmailAdmin);
            this.Controls.Add(this.buttonInstagramAdmin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonFacebookAdmin);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.buttonLoginAdmin);
            this.Controls.Add(this.textBoxLoginAdminPassword);
            this.Controls.Add(this.textBoxLoginAdminUserName);
            this.Controls.Add(this.panelLoginAdmin);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LoginAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelLoginAdmin.ResumeLayout(false);
            this.panelLoginAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelLoginAdmin;
        private System.Windows.Forms.Button buttonLoginAdminMinimized;
        private System.Windows.Forms.Button buttonLoginAdminExit;
        private System.Windows.Forms.Label labelLoginAdminTime;
        private System.Windows.Forms.Timer timerLoginAdmin;
        private System.Windows.Forms.TextBox textBoxLoginAdminUserName;
        private System.Windows.Forms.TextBox textBoxLoginAdminPassword;
        private System.Windows.Forms.Button buttonLoginAdmin;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button buttonFacebookAdmin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonInstagramAdmin;
        private System.Windows.Forms.Button buttonGmailAdmin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel linkLabelForgotLoginAdmin;
        private System.Windows.Forms.PictureBox pictureBoxEye;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}