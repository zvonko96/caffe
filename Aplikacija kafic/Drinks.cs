﻿using System;
using BL;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DL.Models;
using System.Data.SqlClient;

namespace Aplikacija_kafic
{
    public partial class Settings : Form
    {
        DrinkBusiness drinkBusiness;
        
        public void Get()
        {
            
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlDataAdapter sda = new SqlDataAdapter("select * from Drinks", conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            dataGridViewDrink.DataSource = dt;
        }

        public Settings()
        {
            InitializeComponent();
            this.drinkBusiness = new DrinkBusiness();
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login la = new Login();
            la.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //List<Drink> listOfDrinks = this.drinkBusiness.GetDrinks();

            //foreach (Drink d in listOfDrinks)
            //{
            //    listBoxAllDrinks.Items.Add(d.IdDrink + ". " + d.DrinkName + " " + d.Quantity + " " + d.UnitOfMeasure + " " + d.Price + " " + d.Currency);
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonDrinks_Click(object sender, EventArgs e)
        {
                    buttonDrinks.BackColor = Color.Khaki;
                    buttonFoods.BackColor = Color.Gainsboro;
                    buttonOrders.BackColor = Color.Gainsboro;
                    buttonOrderItems.BackColor = Color.Gainsboro;
                    buttonUsers.BackColor = Color.Gainsboro;

            this.Show();
            Foods f = new Foods();
            f.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Hide();

        }

       

        private void buttonFoods_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Foods f = new Foods();
            f.Show();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Hide();

        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Khaki;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;
            this.Hide();
            Foods f = new Foods();
            f.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Show();
            OrderItems i = new OrderItems();
            i.Hide();
        }

        private void buttonOrderItems_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Khaki;
            buttonUsers.BackColor = Color.Gainsboro;
            this.Hide();
            Foods f = new Foods();
            f.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Show();

        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Khaki;
            this.Hide();
            Foods f = new Foods();
            f.Hide();
            Users u = new Users();
            u.Show();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Hide();

        }

        

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Login l = new Login();
            l.Show();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
           
            buttonDrinks.BackColor = Color.Khaki;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Get();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        

        private void buttonAddDrink_Click(object sender, EventArgs e)
        {
            
            string Str1 = textBoxQuantityDrink.Text.Trim();
            double Num1;
            bool isNum1 = double.TryParse(Str1, out Num1);

            string Str2 = textBoxPriceDrink.Text.Trim();
            int Num2;
            bool isNum2 = int.TryParse(Str2, out Num2);

            if (textBoxDrinkName.Text != "" && textBoxQuantityDrink.Text != "" && textBoxUnitOfMeasureDrink.Text != "" && textBoxPriceDrink.Text != "" && textBoxCurrencyDrink.Text != "" && comboBoxCategoryDrink.Text != "")
            {
                Drink d = new Drink();
                if (isNum1 && isNum2)
                {
                    d.DrinkName = textBoxDrinkName.Text;
                    d.Quantity = Convert.ToDouble(textBoxQuantityDrink.Text);
                    d.UnitOfMeasure = textBoxUnitOfMeasureDrink.Text;
                    d.Price = Convert.ToInt32(textBoxPriceDrink.Text);
                    d.Currency = textBoxCurrencyDrink.Text;
                    d.CategoryDrink = comboBoxCategoryDrink.Text;

                    this.drinkBusiness.NewDrink(d);
                    this.Get();
                }
                else
                {
                    MessageBox.Show("IdPica i cena moraju biti celi brojevi, kolicina moze biti realni broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void buttonDeleteDrink_Click(object sender, EventArgs e)
        {
            string Str = textBoxIdDrink.Text.Trim();
            int Num;
            bool isNum = int.TryParse(Str, out Num);

            if (textBoxIdDrink.Text != "")
            {
                Drink d = new Drink();
                if (isNum)
                {
                    d.IdDrink = Convert.ToInt32(textBoxIdDrink.Text);
                    this.drinkBusiness.DeleteDrink(d);

                    this.Get();
                }
                else
                {
                    MessageBox.Show("IdPica i cena moraju biti celi brojevi, kolicina moze biti realni broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate uneti IdPica ako zelite da obrisete!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void buttonEditDrink_Click(object sender, EventArgs e)
        {
            string Str = textBoxIdDrink.Text.Trim();
            int Num;
            bool isNum = int.TryParse(Str, out Num);

            string Str1 = textBoxQuantityDrink.Text.Trim();
            double Num1;
            bool isNum1 = double.TryParse(Str1, out Num1);

            string Str2 = textBoxPriceDrink.Text.Trim();
            int Num2;
            bool isNum2 = int.TryParse(Str2, out Num2);

            if (textBoxIdDrink.Text != "" && textBoxDrinkName.Text != "" && textBoxQuantityDrink.Text != "" && textBoxUnitOfMeasureDrink.Text != "" && textBoxPriceDrink.Text != "" && textBoxCurrencyDrink.Text != "" && comboBoxCategoryDrink.Text != "")
            {
                Drink d = new Drink();
                if (isNum && isNum1 && isNum2)
                {
                    d.IdDrink = Convert.ToInt32(textBoxIdDrink.Text);
                    d.DrinkName = textBoxDrinkName.Text;
                    d.Quantity = Convert.ToDouble(textBoxQuantityDrink.Text);
                    d.UnitOfMeasure = textBoxUnitOfMeasureDrink.Text;
                    d.Price = Convert.ToInt32(textBoxPriceDrink.Text);
                    d.Currency = textBoxCurrencyDrink.Text;
                    d.CategoryDrink = comboBoxCategoryDrink.Text;

                    this.drinkBusiness.UpdateDrink(d);

                    this.Get();
                }
                else
                {
                    {
                        MessageBox.Show("IdPica i cena moraju biti celi brojevi, kolicina moze biti realni broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

       



        

        private void textBoxDrinkName_TextChanged(object sender, EventArgs e)
        {
            if (textBoxDrinkName.Text == "")
            {
                label1.Show();
            }
            else
            {
                label1.Hide();
            }
        }

        private void textBoxQuantityDrink_TextChanged(object sender, EventArgs e)
        {
            if (textBoxQuantityDrink.Text == "")
            {
                label2.Show();
            }
            else
            {
                label2.Hide();
            }
        }

        private void textBoxUnitOfMeasureDrink_TextChanged(object sender, EventArgs e)
        {
            if (textBoxUnitOfMeasureDrink.Text == "")
            {
                label3.Show();
            }
            else
            {
                label3.Hide();
            }
        }

        private void textBoxPriceDrink_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPriceDrink.Text == "")
            {
                label4.Show();
            }
            else
            {
                label4.Hide();
            }
        }

        private void textBoxCurrencyDrink_TextChanged(object sender, EventArgs e)
        {
            if (textBoxCurrencyDrink.Text == "")
            {
                label6.Show();
            }
            else
            {
                label6.Hide();
            }
        }

        private void dataGridViewDrink_Click(object sender, EventArgs e)
        {
            textBoxIdDrink.Text = this.dataGridViewDrink.CurrentRow.Cells[0].Value.ToString();
            textBoxDrinkName.Text = this.dataGridViewDrink.CurrentRow.Cells[1].Value.ToString();
            textBoxQuantityDrink.Text = this.dataGridViewDrink.CurrentRow.Cells[2].Value.ToString();
            textBoxUnitOfMeasureDrink.Text = this.dataGridViewDrink.CurrentRow.Cells[3].Value.ToString();
            textBoxPriceDrink.Text = this.dataGridViewDrink.CurrentRow.Cells[4].Value.ToString();
            textBoxCurrencyDrink.Text = this.dataGridViewDrink.CurrentRow.Cells[5].Value.ToString();
            comboBoxCategoryDrink.Text = this.dataGridViewDrink.CurrentRow.Cells[6].Value.ToString();
        }

        private void comboBoxCategoryDrink_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCategoryDrink.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide();
            }
        }

        private void dataGridViewDrink_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panelDrinks_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
