﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    public partial class googleLogin : Form
    {
        public googleLogin()
        {
            InitializeComponent();
        }

        private void emailLogin_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxEmailGoogle.Text != "" && textBoxPasswordGoogle.Text != "")
            {
                SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlDataAdapter sda = new SqlDataAdapter("select isAdmin from [User] where Email='" + textBoxEmailGoogle.Text + "' and PasswordEmail='" + textBoxPasswordGoogle.Text + "'", conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count == 1)
            {

                if (dt.Rows[0][0].ToString() == "True")
                {
                    this.Hide();
                    Settings mm = new Settings();
                    mm.Show();
                }
                else
                {
                    this.Hide();
                    Waiter wt = new Waiter();
                    wt.Show();
                }

            }
            else
            {
                MessageBox.Show("Pogresno ste uneli email ili lozinku!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                label1.Show();
            }

        }
        bool durum;
        private void pictureBoxEye_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxPasswordGoogle.UseSystemPasswordChar = false;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxPasswordGoogle.UseSystemPasswordChar = true;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void textBoxEmailGoogle_TextChanged(object sender, EventArgs e)
        {
            if (textBoxEmailGoogle.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide();
            }
        }

        private void textBoxPasswordGoogle_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPasswordGoogle.Text == "")
            {
                label1.Show();
            }
            else
            {
                label1.Hide();
            }
        }
    }
}
