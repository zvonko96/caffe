﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;
using DL.Models;

namespace Aplikacija_kafic
{
    public partial class OrderItems : Form
    {
        OrderItemsBusiness orderItemsBusiness;

        public OrderItems()
        {
            InitializeComponent();
            this.orderItemsBusiness = new OrderItemsBusiness();
        }

        public void Get()
        {

            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlDataAdapter sda = new SqlDataAdapter("select * from [OrderItems]", conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            dataGridViewOrderItems.DataSource = dt;
        }

        private void buttonDrinks_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Khaki;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Show();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            Foods i = new Foods();
            i.Hide();
        }

        private void buttonFoods_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            Foods i = new Foods();
            i.Show();
        }

        private void buttonOrderItems_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Khaki;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Show();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            Foods i = new Foods();
            i.Hide();
        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Khaki;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Show();
            Orders o = new Orders();
            o.Hide();
            Foods i = new Foods();
            i.Hide();
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login l = new Login();
            l.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OrderItems_Load(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Khaki;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Get();
        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Khaki;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Show();
            Foods i = new Foods();
            i.Hide();
        }

        private void buttonAddFood_Click(object sender, EventArgs e)
        {
            string Str = textBoxIdDrinks.Text.Trim();
            double Num;
            bool isNum = double.TryParse(Str, out Num);

            string Str1 = textBoxIdFood.Text.Trim();
            double Num1;
            bool isNum1 = double.TryParse(Str1, out Num1);

            string Str2 = textBoxIdOrders.Text.Trim();
            int Num2;
            bool isNum2 = int.TryParse(Str2, out Num2);

            if (textBoxIdDrinks.Text != "" && textBoxIdFood.Text != "" && textBoxIdOrders.Text != "" )
            {
                OrderItem d = new OrderItem();
                if (isNum && isNum1 && isNum2)
                {
                    d.IdDrink = Convert.ToInt32(textBoxIdDrinks.Text);
                    d.IdFood = Convert.ToInt32(textBoxIdFood.Text);
                    d.IdOrder = Convert.ToInt32(textBoxIdOrders.Text);

                    this.orderItemsBusiness.NewOrderItem(d);
                    this.Get();
                }
                else
                {
                    MessageBox.Show("U poljima morate uneti cele brojeve!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void textBoxIdDrinks_TextChanged(object sender, EventArgs e)
        {
            if (textBoxIdDrinks.Text == "")
            {
                label1.Show();
            }
            else
            {
                label1.Hide();
            }
        }

        private void textBoxIdFood_TextChanged(object sender, EventArgs e)
        {
            if (textBoxIdFood.Text == "")
            {
                label2.Show();
            }
            else
            {
                label2.Hide();
            }
        }

        private void textBoxIdOrders_TextChanged(object sender, EventArgs e)
        {
            if (textBoxIdOrders.Text == "")
            {
                label3.Show();
            }
            else
            {
                label3.Hide();
            }
        }

        private void dataGridViewOrderItems_Click(object sender, EventArgs e)
        {
            textBoxIdOrderItems.Text = this.dataGridViewOrderItems.CurrentRow.Cells[0].Value.ToString();
            textBoxIdDrinks.Text = this.dataGridViewOrderItems.CurrentRow.Cells[1].Value.ToString();
            textBoxIdFood.Text = this.dataGridViewOrderItems.CurrentRow.Cells[2].Value.ToString();
            textBoxIdOrders.Text = this.dataGridViewOrderItems.CurrentRow.Cells[3].Value.ToString();
           
        }

        private void buttonDeleteFood_Click(object sender, EventArgs e)
        {
            string Str = textBoxIdOrderItems.Text.Trim();
            int Num;
            bool isNum = int.TryParse(Str, out Num);

            if (textBoxIdOrderItems.Text != "")
            {
                OrderItem d = new OrderItem();
                if (isNum)
                {
                    d.IdItem = Convert.ToInt32(textBoxIdOrderItems.Text);
                    this.orderItemsBusiness.DeleteOrderItem(d);

                    this.Get();
                }
                else
                {
                    MessageBox.Show("IdStavke mora biti ceo broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate uneti idStavke ako zelite da obrisete!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void buttonEditFood_Click(object sender, EventArgs e)
        {
            string Str3 = textBoxIdOrderItems.Text.Trim();
            int Num3;
            bool isNum3 = int.TryParse(Str3, out Num3);

            string Str = textBoxIdDrinks.Text.Trim();
            double Num;
            bool isNum = double.TryParse(Str, out Num);

            string Str1 = textBoxIdFood.Text.Trim();
            double Num1;
            bool isNum1 = double.TryParse(Str1, out Num1);

            string Str2 = textBoxIdOrders.Text.Trim();
            int Num2;
            bool isNum2 = int.TryParse(Str2, out Num2);

            if (textBoxIdOrderItems.Text != "" && textBoxIdDrinks.Text != "" && textBoxIdFood.Text != "" && textBoxIdOrders.Text != "")
            {
                OrderItem d = new OrderItem();
                if (isNum3 && isNum && isNum1 && isNum2)
                {
                    d.IdItem = Convert.ToInt32(textBoxIdOrderItems.Text);
                    d.IdDrink = Convert.ToInt32(textBoxIdDrinks.Text);
                    d.IdFood = Convert.ToInt32(textBoxIdFood.Text);
                    d.IdOrder = Convert.ToInt32(textBoxIdOrders.Text);

                    this.orderItemsBusiness.UpdateOrderItems(d);
                    this.Get();
                }
                else
                {
                    MessageBox.Show("U poljima morate uneti cele brojeve!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridViewOrderItems_Click_1(object sender, EventArgs e)
        {
            textBoxIdOrderItems.Text = this.dataGridViewOrderItems.CurrentRow.Cells[0].Value.ToString();
            textBoxIdDrinks.Text = this.dataGridViewOrderItems.CurrentRow.Cells[1].Value.ToString();
            textBoxIdFood.Text = this.dataGridViewOrderItems.CurrentRow.Cells[2].Value.ToString();
            textBoxIdOrders.Text = this.dataGridViewOrderItems.CurrentRow.Cells[3].Value.ToString();
        }
    }
}
