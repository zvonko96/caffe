﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    public partial class Users : Form
    {
        public Users()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonDrinks_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Khaki;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Show();
            Orders u = new Orders();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonFoods_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Orders u = new Orders();
            u.Hide();
            Foods f = new Foods();
            f.Show();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Khaki;

            this.Show();
            Settings drink = new Settings();
            drink.Hide();
            Orders u = new Orders();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login f = new Login();
            f.Hide();
        }

        private void Users_Load(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Khaki;
        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Khaki;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Orders u = new Orders();
            u.Show();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonOrderItems_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Khaki;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Orders u = new Orders();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Show();
        }
    }
}
