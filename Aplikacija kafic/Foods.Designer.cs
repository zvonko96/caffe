﻿namespace Aplikacija_kafic
{
    partial class Foods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Foods));
            this.label3 = new System.Windows.Forms.Label();
            this.labelCategoryFood = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxCategoryFood = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelIdFood = new System.Windows.Forms.Label();
            this.textBoxIdFood = new System.Windows.Forms.TextBox();
            this.labelCurrencyFood = new System.Windows.Forms.Label();
            this.labelPriceFood = new System.Windows.Forms.Label();
            this.labelUnitOfMeasureFood = new System.Windows.Forms.Label();
            this.textBoxPriceFood = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelQuantityFood = new System.Windows.Forms.Label();
            this.labelFoodName = new System.Windows.Forms.Label();
            this.textBoxFoodName = new System.Windows.Forms.TextBox();
            this.textBoxQuantityFood = new System.Windows.Forms.TextBox();
            this.textBoxUnitOfMeasureFood = new System.Windows.Forms.TextBox();
            this.dataGridViewFood = new System.Windows.Forms.DataGridView();
            this.intIdJela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNazivJela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtJedinicaMere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.intPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValuta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbKategorija = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.panelDrinks = new System.Windows.Forms.Panel();
            this.pictureBoxDrink = new System.Windows.Forms.PictureBox();
            this.buttonEditFood = new System.Windows.Forms.Button();
            this.buttonDeleteFood = new System.Windows.Forms.Button();
            this.buttonAddFood = new System.Windows.Forms.Button();
            this.textBoxCurrencyFood = new System.Windows.Forms.TextBox();
            this.buttonUsers = new System.Windows.Forms.Button();
            this.buttonOrderItems = new System.Windows.Forms.Button();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonFoods = new System.Windows.Forms.Button();
            this.buttonDrinks = new System.Windows.Forms.Button();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFood)).BeginInit();
            this.panelDrinks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(493, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 26);
            this.label3.TabIndex = 27;
            this.label3.Text = "*";
            // 
            // labelCategoryFood
            // 
            this.labelCategoryFood.AutoSize = true;
            this.labelCategoryFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoryFood.Location = new System.Drawing.Point(73, 370);
            this.labelCategoryFood.Name = "labelCategoryFood";
            this.labelCategoryFood.Size = new System.Drawing.Size(94, 18);
            this.labelCategoryFood.TabIndex = 32;
            this.labelCategoryFood.Text = "Kategorija: ";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(493, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 26);
            this.label5.TabIndex = 31;
            this.label5.Text = "*";
            // 
            // comboBoxCategoryFood
            // 
            this.comboBoxCategoryFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.comboBoxCategoryFood.FormattingEnabled = true;
            this.comboBoxCategoryFood.Items.AddRange(new object[] {
            "Ribe",
            "Rostilj",
            "Corbice",
            "Kolaci"});
            this.comboBoxCategoryFood.Location = new System.Drawing.Point(202, 367);
            this.comboBoxCategoryFood.Name = "comboBoxCategoryFood";
            this.comboBoxCategoryFood.Size = new System.Drawing.Size(285, 26);
            this.comboBoxCategoryFood.TabIndex = 30;
            this.comboBoxCategoryFood.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategoryFood_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(493, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 26);
            this.label6.TabIndex = 29;
            this.label6.Text = "*";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(493, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 26);
            this.label4.TabIndex = 28;
            this.label4.Text = "*";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(493, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 26);
            this.label2.TabIndex = 26;
            this.label2.Text = "*";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(493, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 26);
            this.label1.TabIndex = 25;
            this.label1.Text = "*";
            // 
            // labelIdFood
            // 
            this.labelIdFood.AutoSize = true;
            this.labelIdFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdFood.Location = new System.Drawing.Point(73, 74);
            this.labelIdFood.Name = "labelIdFood";
            this.labelIdFood.Size = new System.Drawing.Size(62, 18);
            this.labelIdFood.TabIndex = 18;
            this.labelIdFood.Text = "Id jela: ";
            this.labelIdFood.Visible = false;
            // 
            // textBoxIdFood
            // 
            this.textBoxIdFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBoxIdFood.Location = new System.Drawing.Point(202, 71);
            this.textBoxIdFood.Name = "textBoxIdFood";
            this.textBoxIdFood.Size = new System.Drawing.Size(285, 24);
            this.textBoxIdFood.TabIndex = 17;
            this.textBoxIdFood.Visible = false;
            // 
            // labelCurrencyFood
            // 
            this.labelCurrencyFood.AutoSize = true;
            this.labelCurrencyFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrencyFood.Location = new System.Drawing.Point(73, 324);
            this.labelCurrencyFood.Name = "labelCurrencyFood";
            this.labelCurrencyFood.Size = new System.Drawing.Size(64, 18);
            this.labelCurrencyFood.TabIndex = 16;
            this.labelCurrencyFood.Text = "Valuta: ";
            // 
            // labelPriceFood
            // 
            this.labelPriceFood.AutoSize = true;
            this.labelPriceFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPriceFood.Location = new System.Drawing.Point(73, 271);
            this.labelPriceFood.Name = "labelPriceFood";
            this.labelPriceFood.Size = new System.Drawing.Size(57, 18);
            this.labelPriceFood.TabIndex = 15;
            this.labelPriceFood.Text = "Cena: ";
            // 
            // labelUnitOfMeasureFood
            // 
            this.labelUnitOfMeasureFood.AutoSize = true;
            this.labelUnitOfMeasureFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnitOfMeasureFood.Location = new System.Drawing.Point(73, 226);
            this.labelUnitOfMeasureFood.Name = "labelUnitOfMeasureFood";
            this.labelUnitOfMeasureFood.Size = new System.Drawing.Size(123, 18);
            this.labelUnitOfMeasureFood.TabIndex = 14;
            this.labelUnitOfMeasureFood.Text = "Jedinica mere: ";
            // 
            // textBoxPriceFood
            // 
            this.textBoxPriceFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPriceFood.Location = new System.Drawing.Point(202, 268);
            this.textBoxPriceFood.Name = "textBoxPriceFood";
            this.textBoxPriceFood.Size = new System.Drawing.Size(285, 24);
            this.textBoxPriceFood.TabIndex = 4;
            this.textBoxPriceFood.TextChanged += new System.EventHandler(this.textBoxPriceFood_TextChanged);
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(1326, 2);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(38, 28);
            this.buttonClose.TabIndex = 9;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelQuantityFood
            // 
            this.labelQuantityFood.AutoSize = true;
            this.labelQuantityFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuantityFood.Location = new System.Drawing.Point(73, 174);
            this.labelQuantityFood.Name = "labelQuantityFood";
            this.labelQuantityFood.Size = new System.Drawing.Size(78, 18);
            this.labelQuantityFood.TabIndex = 13;
            this.labelQuantityFood.Text = "Kolicina: ";
            // 
            // labelFoodName
            // 
            this.labelFoodName.AutoSize = true;
            this.labelFoodName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFoodName.Location = new System.Drawing.Point(73, 122);
            this.labelFoodName.Name = "labelFoodName";
            this.labelFoodName.Size = new System.Drawing.Size(91, 18);
            this.labelFoodName.TabIndex = 12;
            this.labelFoodName.Text = "Naziv jela: ";
            // 
            // textBoxFoodName
            // 
            this.textBoxFoodName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFoodName.Location = new System.Drawing.Point(202, 119);
            this.textBoxFoodName.Name = "textBoxFoodName";
            this.textBoxFoodName.Size = new System.Drawing.Size(285, 24);
            this.textBoxFoodName.TabIndex = 1;
            this.textBoxFoodName.TextChanged += new System.EventHandler(this.textBoxFoodName_TextChanged);
            // 
            // textBoxQuantityFood
            // 
            this.textBoxQuantityFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQuantityFood.Location = new System.Drawing.Point(202, 171);
            this.textBoxQuantityFood.Name = "textBoxQuantityFood";
            this.textBoxQuantityFood.Size = new System.Drawing.Size(285, 24);
            this.textBoxQuantityFood.TabIndex = 2;
            this.textBoxQuantityFood.TextChanged += new System.EventHandler(this.textBoxQuantityFood_TextChanged);
            // 
            // textBoxUnitOfMeasureFood
            // 
            this.textBoxUnitOfMeasureFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUnitOfMeasureFood.Location = new System.Drawing.Point(202, 223);
            this.textBoxUnitOfMeasureFood.Name = "textBoxUnitOfMeasureFood";
            this.textBoxUnitOfMeasureFood.Size = new System.Drawing.Size(285, 24);
            this.textBoxUnitOfMeasureFood.TabIndex = 3;
            this.textBoxUnitOfMeasureFood.TextChanged += new System.EventHandler(this.textBoxUnitOfMeasureFood_TextChanged);
            // 
            // dataGridViewFood
            // 
            this.dataGridViewFood.AllowUserToOrderColumns = true;
            this.dataGridViewFood.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFood.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.intIdJela,
            this.txtNazivJela,
            this.txtKolicina,
            this.txtJedinicaMere,
            this.intPrice,
            this.txtValuta,
            this.cmbKategorija});
            this.dataGridViewFood.Location = new System.Drawing.Point(1, 485);
            this.dataGridViewFood.Name = "dataGridViewFood";
            this.dataGridViewFood.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewFood.Size = new System.Drawing.Size(1170, 251);
            this.dataGridViewFood.TabIndex = 0;
            this.dataGridViewFood.Click += new System.EventHandler(this.dataGridViewFood_Click);
            // 
            // intIdJela
            // 
            this.intIdJela.DataPropertyName = "IdFood";
            this.intIdJela.FillWeight = 75.00001F;
            this.intIdJela.HeaderText = "IdJela";
            this.intIdJela.Name = "intIdJela";
            // 
            // txtNazivJela
            // 
            this.txtNazivJela.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtNazivJela.DataPropertyName = "FoodName";
            this.txtNazivJela.FillWeight = 83.33333F;
            this.txtNazivJela.HeaderText = "NazivJela";
            this.txtNazivJela.Name = "txtNazivJela";
            // 
            // txtKolicina
            // 
            this.txtKolicina.DataPropertyName = "Quantity";
            this.txtKolicina.FillWeight = 41.66666F;
            this.txtKolicina.HeaderText = "Kolicina";
            this.txtKolicina.Name = "txtKolicina";
            this.txtKolicina.Width = 150;
            // 
            // txtJedinicaMere
            // 
            this.txtJedinicaMere.DataPropertyName = "UnitOfMeasure";
            this.txtJedinicaMere.HeaderText = "JedinicaMere";
            this.txtJedinicaMere.Name = "txtJedinicaMere";
            this.txtJedinicaMere.Width = 150;
            // 
            // intPrice
            // 
            this.intPrice.DataPropertyName = "Price";
            this.intPrice.FillWeight = 25F;
            this.intPrice.HeaderText = "Cena";
            this.intPrice.Name = "intPrice";
            this.intPrice.Width = 150;
            // 
            // txtValuta
            // 
            this.txtValuta.DataPropertyName = "Currency";
            this.txtValuta.FillWeight = 20F;
            this.txtValuta.HeaderText = "Valuta";
            this.txtValuta.Name = "txtValuta";
            // 
            // cmbKategorija
            // 
            this.cmbKategorija.DataPropertyName = "CategoryFood";
            this.cmbKategorija.HeaderText = "Kategorija";
            this.cmbKategorija.Name = "cmbKategorija";
            this.cmbKategorija.Width = 150;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1290, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 28);
            this.button1.TabIndex = 10;
            this.button1.Text = "_";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelDrinks
            // 
            this.panelDrinks.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelDrinks.Controls.Add(this.labelCategoryFood);
            this.panelDrinks.Controls.Add(this.label5);
            this.panelDrinks.Controls.Add(this.comboBoxCategoryFood);
            this.panelDrinks.Controls.Add(this.label6);
            this.panelDrinks.Controls.Add(this.label4);
            this.panelDrinks.Controls.Add(this.label3);
            this.panelDrinks.Controls.Add(this.label2);
            this.panelDrinks.Controls.Add(this.label1);
            this.panelDrinks.Controls.Add(this.labelIdFood);
            this.panelDrinks.Controls.Add(this.textBoxIdFood);
            this.panelDrinks.Controls.Add(this.labelCurrencyFood);
            this.panelDrinks.Controls.Add(this.labelPriceFood);
            this.panelDrinks.Controls.Add(this.labelUnitOfMeasureFood);
            this.panelDrinks.Controls.Add(this.labelQuantityFood);
            this.panelDrinks.Controls.Add(this.labelFoodName);
            this.panelDrinks.Controls.Add(this.textBoxFoodName);
            this.panelDrinks.Controls.Add(this.textBoxQuantityFood);
            this.panelDrinks.Controls.Add(this.textBoxUnitOfMeasureFood);
            this.panelDrinks.Controls.Add(this.dataGridViewFood);
            this.panelDrinks.Controls.Add(this.textBoxPriceFood);
            this.panelDrinks.Controls.Add(this.pictureBoxDrink);
            this.panelDrinks.Controls.Add(this.buttonEditFood);
            this.panelDrinks.Controls.Add(this.buttonDeleteFood);
            this.panelDrinks.Controls.Add(this.buttonAddFood);
            this.panelDrinks.Controls.Add(this.textBoxCurrencyFood);
            this.panelDrinks.Location = new System.Drawing.Point(193, 36);
            this.panelDrinks.Name = "panelDrinks";
            this.panelDrinks.Size = new System.Drawing.Size(1173, 731);
            this.panelDrinks.TabIndex = 8;
            // 
            // pictureBoxDrink
            // 
            this.pictureBoxDrink.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDrink.BackgroundImage")));
            this.pictureBoxDrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDrink.Location = new System.Drawing.Point(548, 102);
            this.pictureBoxDrink.Name = "pictureBoxDrink";
            this.pictureBoxDrink.Size = new System.Drawing.Size(587, 259);
            this.pictureBoxDrink.TabIndex = 11;
            this.pictureBoxDrink.TabStop = false;
            // 
            // buttonEditFood
            // 
            this.buttonEditFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditFood.Location = new System.Drawing.Point(400, 412);
            this.buttonEditFood.Name = "buttonEditFood";
            this.buttonEditFood.Size = new System.Drawing.Size(87, 36);
            this.buttonEditFood.TabIndex = 8;
            this.buttonEditFood.Text = "Izmeni";
            this.buttonEditFood.UseVisualStyleBackColor = true;
            this.buttonEditFood.Click += new System.EventHandler(this.buttonEditDrink_Click);
            // 
            // buttonDeleteFood
            // 
            this.buttonDeleteFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteFood.Location = new System.Drawing.Point(304, 412);
            this.buttonDeleteFood.Name = "buttonDeleteFood";
            this.buttonDeleteFood.Size = new System.Drawing.Size(90, 36);
            this.buttonDeleteFood.TabIndex = 7;
            this.buttonDeleteFood.Text = "Obrisi";
            this.buttonDeleteFood.UseVisualStyleBackColor = true;
            this.buttonDeleteFood.Click += new System.EventHandler(this.buttonDeleteDrink_Click);
            // 
            // buttonAddFood
            // 
            this.buttonAddFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddFood.Location = new System.Drawing.Point(202, 412);
            this.buttonAddFood.Name = "buttonAddFood";
            this.buttonAddFood.Size = new System.Drawing.Size(88, 36);
            this.buttonAddFood.TabIndex = 6;
            this.buttonAddFood.Text = "Dodaj";
            this.buttonAddFood.UseVisualStyleBackColor = true;
            this.buttonAddFood.Click += new System.EventHandler(this.buttonAddDrink_Click);
            // 
            // textBoxCurrencyFood
            // 
            this.textBoxCurrencyFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCurrencyFood.Location = new System.Drawing.Point(202, 321);
            this.textBoxCurrencyFood.Name = "textBoxCurrencyFood";
            this.textBoxCurrencyFood.Size = new System.Drawing.Size(285, 24);
            this.textBoxCurrencyFood.TabIndex = 5;
            this.textBoxCurrencyFood.TextChanged += new System.EventHandler(this.textBoxCurrencyFood_TextChanged);
            // 
            // buttonUsers
            // 
            this.buttonUsers.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUsers.Location = new System.Drawing.Point(5, 211);
            this.buttonUsers.Name = "buttonUsers";
            this.buttonUsers.Size = new System.Drawing.Size(180, 52);
            this.buttonUsers.TabIndex = 4;
            this.buttonUsers.Text = "Korisnici";
            this.buttonUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonUsers.UseVisualStyleBackColor = false;
            this.buttonUsers.Click += new System.EventHandler(this.buttonUsers_Click);
            // 
            // buttonOrderItems
            // 
            this.buttonOrderItems.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrderItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrderItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrderItems.Location = new System.Drawing.Point(5, 159);
            this.buttonOrderItems.Name = "buttonOrderItems";
            this.buttonOrderItems.Size = new System.Drawing.Size(180, 52);
            this.buttonOrderItems.TabIndex = 3;
            this.buttonOrderItems.Text = "Stavke narudzbine";
            this.buttonOrderItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOrderItems.UseVisualStyleBackColor = false;
            this.buttonOrderItems.Click += new System.EventHandler(this.buttonOrderItems_Click);
            // 
            // buttonOrders
            // 
            this.buttonOrders.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrders.Location = new System.Drawing.Point(5, 107);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Size = new System.Drawing.Size(180, 52);
            this.buttonOrders.TabIndex = 2;
            this.buttonOrders.Text = "Narudzbine";
            this.buttonOrders.UseVisualStyleBackColor = false;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonFoods
            // 
            this.buttonFoods.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonFoods.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFoods.Location = new System.Drawing.Point(5, 55);
            this.buttonFoods.Name = "buttonFoods";
            this.buttonFoods.Size = new System.Drawing.Size(180, 52);
            this.buttonFoods.TabIndex = 1;
            this.buttonFoods.Text = "Jela";
            this.buttonFoods.UseVisualStyleBackColor = false;
            this.buttonFoods.Click += new System.EventHandler(this.buttonFoods_Click);
            // 
            // buttonDrinks
            // 
            this.buttonDrinks.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonDrinks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDrinks.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonDrinks.Location = new System.Drawing.Point(5, 3);
            this.buttonDrinks.Name = "buttonDrinks";
            this.buttonDrinks.Size = new System.Drawing.Size(180, 52);
            this.buttonDrinks.TabIndex = 0;
            this.buttonDrinks.Text = "Pica";
            this.buttonDrinks.UseVisualStyleBackColor = false;
            this.buttonDrinks.Click += new System.EventHandler(this.buttonDrinks_Click);
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogOut.Location = new System.Drawing.Point(5, 705);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(180, 52);
            this.buttonLogOut.TabIndex = 7;
            this.buttonLogOut.Text = "Log out";
            this.buttonLogOut.UseVisualStyleBackColor = true;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelMenu.Controls.Add(this.buttonLogOut);
            this.panelMenu.Controls.Add(this.buttonUsers);
            this.panelMenu.Controls.Add(this.buttonOrderItems);
            this.panelMenu.Controls.Add(this.buttonOrders);
            this.panelMenu.Controls.Add(this.buttonFoods);
            this.panelMenu.Controls.Add(this.buttonDrinks);
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(192, 772);
            this.panelMenu.TabIndex = 7;
            // 
            // Foods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panelDrinks);
            this.Controls.Add(this.panelMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Foods";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Foods";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Foods_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFood)).EndInit();
            this.panelDrinks.ResumeLayout(false);
            this.panelDrinks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelCategoryFood;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxCategoryFood;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelIdFood;
        private System.Windows.Forms.TextBox textBoxIdFood;
        private System.Windows.Forms.Label labelCurrencyFood;
        private System.Windows.Forms.Label labelPriceFood;
        private System.Windows.Forms.Label labelUnitOfMeasureFood;
        private System.Windows.Forms.TextBox textBoxPriceFood;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelQuantityFood;
        private System.Windows.Forms.Label labelFoodName;
        private System.Windows.Forms.TextBox textBoxFoodName;
        private System.Windows.Forms.TextBox textBoxQuantityFood;
        private System.Windows.Forms.TextBox textBoxUnitOfMeasureFood;
        private System.Windows.Forms.DataGridView dataGridViewFood;
        private System.Windows.Forms.PictureBox pictureBoxDrink;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelDrinks;
        private System.Windows.Forms.Button buttonEditFood;
        private System.Windows.Forms.Button buttonDeleteFood;
        private System.Windows.Forms.Button buttonAddFood;
        private System.Windows.Forms.TextBox textBoxCurrencyFood;
        private System.Windows.Forms.Button buttonUsers;
        private System.Windows.Forms.Button buttonOrderItems;
        private System.Windows.Forms.Button buttonOrders;
        private System.Windows.Forms.Button buttonFoods;
        private System.Windows.Forms.Button buttonDrinks;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.DataGridViewTextBoxColumn intIdJela;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNazivJela;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKolicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtJedinicaMere;
        private System.Windows.Forms.DataGridViewTextBoxColumn intPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtValuta;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmbKategorija;
    }
}