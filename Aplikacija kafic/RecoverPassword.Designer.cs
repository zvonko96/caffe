﻿namespace Aplikacija_kafic
{
    partial class RecoverPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelShoes = new System.Windows.Forms.Label();
            this.textBoxShoes = new System.Windows.Forms.TextBox();
            this.labelFavoriteCity = new System.Windows.Forms.Label();
            this.labelFavoriteFood = new System.Windows.Forms.Label();
            this.textBoxFavoriteCity = new System.Windows.Forms.TextBox();
            this.textBoxFavoriteFood = new System.Windows.Forms.TextBox();
            this.labelUserName = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonRecover = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxChangePasswordConfirm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxChangePassword = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBoxEyeShoes = new System.Windows.Forms.PictureBox();
            this.pictureBoxCity = new System.Windows.Forms.PictureBox();
            this.pictureBoxFood = new System.Windows.Forms.PictureBox();
            this.pictureBoxChangePicture = new System.Windows.Forms.PictureBox();
            this.pictureBoxConfirmPassword = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEyeShoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChangePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConfirmPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // labelShoes
            // 
            this.labelShoes.AutoSize = true;
            this.labelShoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic);
            this.labelShoes.Location = new System.Drawing.Point(12, 64);
            this.labelShoes.Name = "labelShoes";
            this.labelShoes.Size = new System.Drawing.Size(164, 20);
            this.labelShoes.TabIndex = 1;
            this.labelShoes.Text = "Koji je vas broj patika?";
            // 
            // textBoxShoes
            // 
            this.textBoxShoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxShoes.Location = new System.Drawing.Point(16, 87);
            this.textBoxShoes.Name = "textBoxShoes";
            this.textBoxShoes.Size = new System.Drawing.Size(234, 26);
            this.textBoxShoes.TabIndex = 2;
            this.textBoxShoes.UseSystemPasswordChar = true;
            this.textBoxShoes.TextChanged += new System.EventHandler(this.textBoxShoes_TextChanged);
            // 
            // labelFavoriteCity
            // 
            this.labelFavoriteCity.AutoSize = true;
            this.labelFavoriteCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic);
            this.labelFavoriteCity.Location = new System.Drawing.Point(12, 118);
            this.labelFavoriteCity.Name = "labelFavoriteCity";
            this.labelFavoriteCity.Size = new System.Drawing.Size(200, 20);
            this.labelFavoriteCity.TabIndex = 3;
            this.labelFavoriteCity.Text = "Koji je vam je omiljeni grad?";
            // 
            // labelFavoriteFood
            // 
            this.labelFavoriteFood.AutoSize = true;
            this.labelFavoriteFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic);
            this.labelFavoriteFood.Location = new System.Drawing.Point(12, 172);
            this.labelFavoriteFood.Name = "labelFavoriteFood";
            this.labelFavoriteFood.Size = new System.Drawing.Size(205, 20);
            this.labelFavoriteFood.TabIndex = 4;
            this.labelFavoriteFood.Text = "Koja vam je omiljena hrana?";
            // 
            // textBoxFavoriteCity
            // 
            this.textBoxFavoriteCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxFavoriteCity.Location = new System.Drawing.Point(16, 141);
            this.textBoxFavoriteCity.Name = "textBoxFavoriteCity";
            this.textBoxFavoriteCity.Size = new System.Drawing.Size(234, 26);
            this.textBoxFavoriteCity.TabIndex = 5;
            this.textBoxFavoriteCity.UseSystemPasswordChar = true;
            this.textBoxFavoriteCity.TextChanged += new System.EventHandler(this.textBoxFavoriteCity_TextChanged);
            // 
            // textBoxFavoriteFood
            // 
            this.textBoxFavoriteFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxFavoriteFood.Location = new System.Drawing.Point(16, 195);
            this.textBoxFavoriteFood.Name = "textBoxFavoriteFood";
            this.textBoxFavoriteFood.Size = new System.Drawing.Size(235, 26);
            this.textBoxFavoriteFood.TabIndex = 6;
            this.textBoxFavoriteFood.UseSystemPasswordChar = true;
            this.textBoxFavoriteFood.TextChanged += new System.EventHandler(this.textBoxFavoriteFood_TextChanged);
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserName.Location = new System.Drawing.Point(12, 9);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(110, 20);
            this.labelUserName.TabIndex = 7;
            this.labelUserName.Text = "Korisnicko ime";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxUserName.Location = new System.Drawing.Point(16, 32);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(235, 26);
            this.textBoxUserName.TabIndex = 8;
            this.textBoxUserName.TextChanged += new System.EventHandler(this.textBoxUserName_TextChanged);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(257, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 26);
            this.label5.TabIndex = 24;
            this.label5.Text = "*";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(257, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 26);
            this.label1.TabIndex = 25;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(257, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 26);
            this.label2.TabIndex = 26;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(257, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 26);
            this.label3.TabIndex = 27;
            this.label3.Text = "*";
            // 
            // buttonRecover
            // 
            this.buttonRecover.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRecover.Location = new System.Drawing.Point(16, 243);
            this.buttonRecover.Name = "buttonRecover";
            this.buttonRecover.Size = new System.Drawing.Size(234, 39);
            this.buttonRecover.TabIndex = 28;
            this.buttonRecover.Text = "Potvrdi";
            this.buttonRecover.UseVisualStyleBackColor = true;
            this.buttonRecover.Click += new System.EventHandler(this.buttonRecover_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBoxConfirmPassword);
            this.panel1.Controls.Add(this.pictureBoxChangePicture);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBoxChangePasswordConfirm);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxChangePassword);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(0, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(278, 229);
            this.panel1.TabIndex = 29;
            this.panel1.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 20);
            this.label6.TabIndex = 32;
            this.label6.Text = "Potvrdi sifru";
            // 
            // textBoxChangePasswordConfirm
            // 
            this.textBoxChangePasswordConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxChangePasswordConfirm.Location = new System.Drawing.Point(16, 85);
            this.textBoxChangePasswordConfirm.Name = "textBoxChangePasswordConfirm";
            this.textBoxChangePasswordConfirm.Size = new System.Drawing.Size(235, 26);
            this.textBoxChangePasswordConfirm.TabIndex = 31;
            this.textBoxChangePasswordConfirm.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 30;
            this.label4.Text = "Unesite novu sifru";
            // 
            // textBoxChangePassword
            // 
            this.textBoxChangePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBoxChangePassword.Location = new System.Drawing.Point(16, 33);
            this.textBoxChangePassword.Name = "textBoxChangePassword";
            this.textBoxChangePassword.Size = new System.Drawing.Size(235, 26);
            this.textBoxChangePassword.TabIndex = 30;
            this.textBoxChangePassword.UseSystemPasswordChar = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(15, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(235, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Potvrdi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBoxEyeShoes
            // 
            this.pictureBoxEyeShoes.BackColor = System.Drawing.Color.White;
            this.pictureBoxEyeShoes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxEyeShoes.Image = global::Aplikacija_kafic.Properties.Resources.okce;
            this.pictureBoxEyeShoes.Location = new System.Drawing.Point(223, 87);
            this.pictureBoxEyeShoes.Name = "pictureBoxEyeShoes";
            this.pictureBoxEyeShoes.Size = new System.Drawing.Size(28, 26);
            this.pictureBoxEyeShoes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEyeShoes.TabIndex = 45;
            this.pictureBoxEyeShoes.TabStop = false;
            this.pictureBoxEyeShoes.Click += new System.EventHandler(this.pictureBoxEye_Click);
            // 
            // pictureBoxCity
            // 
            this.pictureBoxCity.BackColor = System.Drawing.Color.White;
            this.pictureBoxCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxCity.Image = global::Aplikacija_kafic.Properties.Resources.okce;
            this.pictureBoxCity.Location = new System.Drawing.Point(223, 141);
            this.pictureBoxCity.Name = "pictureBoxCity";
            this.pictureBoxCity.Size = new System.Drawing.Size(28, 26);
            this.pictureBoxCity.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCity.TabIndex = 46;
            this.pictureBoxCity.TabStop = false;
            this.pictureBoxCity.Click += new System.EventHandler(this.pictureBoxCity_Click);
            // 
            // pictureBoxFood
            // 
            this.pictureBoxFood.BackColor = System.Drawing.Color.White;
            this.pictureBoxFood.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxFood.Image = global::Aplikacija_kafic.Properties.Resources.okce;
            this.pictureBoxFood.Location = new System.Drawing.Point(223, 195);
            this.pictureBoxFood.Name = "pictureBoxFood";
            this.pictureBoxFood.Size = new System.Drawing.Size(28, 26);
            this.pictureBoxFood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFood.TabIndex = 47;
            this.pictureBoxFood.TabStop = false;
            this.pictureBoxFood.Click += new System.EventHandler(this.pictureBoxFood_Click);
            // 
            // pictureBoxChangePicture
            // 
            this.pictureBoxChangePicture.BackColor = System.Drawing.Color.White;
            this.pictureBoxChangePicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxChangePicture.Image = global::Aplikacija_kafic.Properties.Resources.okce;
            this.pictureBoxChangePicture.Location = new System.Drawing.Point(223, 33);
            this.pictureBoxChangePicture.Name = "pictureBoxChangePicture";
            this.pictureBoxChangePicture.Size = new System.Drawing.Size(28, 26);
            this.pictureBoxChangePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxChangePicture.TabIndex = 48;
            this.pictureBoxChangePicture.TabStop = false;
            this.pictureBoxChangePicture.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBoxConfirmPassword
            // 
            this.pictureBoxConfirmPassword.BackColor = System.Drawing.Color.White;
            this.pictureBoxConfirmPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxConfirmPassword.Image = global::Aplikacija_kafic.Properties.Resources.okce;
            this.pictureBoxConfirmPassword.Location = new System.Drawing.Point(223, 85);
            this.pictureBoxConfirmPassword.Name = "pictureBoxConfirmPassword";
            this.pictureBoxConfirmPassword.Size = new System.Drawing.Size(28, 26);
            this.pictureBoxConfirmPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxConfirmPassword.TabIndex = 49;
            this.pictureBoxConfirmPassword.TabStop = false;
            this.pictureBoxConfirmPassword.Click += new System.EventHandler(this.pictureBoxConfirmPassword_Click);
            // 
            // RecoverPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 294);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBoxFood);
            this.Controls.Add(this.pictureBoxCity);
            this.Controls.Add(this.pictureBoxEyeShoes);
            this.Controls.Add(this.buttonRecover);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxUserName);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.textBoxFavoriteFood);
            this.Controls.Add(this.textBoxFavoriteCity);
            this.Controls.Add(this.labelFavoriteFood);
            this.Controls.Add(this.labelFavoriteCity);
            this.Controls.Add(this.textBoxShoes);
            this.Controls.Add(this.labelShoes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RecoverPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEyeShoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChangePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConfirmPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelShoes;
        private System.Windows.Forms.TextBox textBoxShoes;
        private System.Windows.Forms.Label labelFavoriteCity;
        private System.Windows.Forms.Label labelFavoriteFood;
        private System.Windows.Forms.TextBox textBoxFavoriteCity;
        private System.Windows.Forms.TextBox textBoxFavoriteFood;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonRecover;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxChangePasswordConfirm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxChangePassword;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBoxEyeShoes;
        private System.Windows.Forms.PictureBox pictureBoxCity;
        private System.Windows.Forms.PictureBox pictureBoxFood;
        private System.Windows.Forms.PictureBox pictureBoxConfirmPassword;
        private System.Windows.Forms.PictureBox pictureBoxChangePicture;
    }
}