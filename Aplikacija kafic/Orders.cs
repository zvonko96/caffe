﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    public partial class Orders : Form
    {
        public Orders()
        {
            InitializeComponent();
        }

        private void buttonDrinks_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Khaki;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Show();
            Users u = new Users();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonFoods_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Foods f = new Foods();
            f.Show();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Khaki;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Show();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonOrderItems_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Khaki;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Show();
        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Khaki;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Show();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void Orders_Load(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Khaki;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Show();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Foods f = new Foods();
            f.Hide();
            OrderItems it = new OrderItems();
            it.Hide();
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login l = new Login();
            l.Show();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
