﻿using BL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    public partial class Foods : Form
    {
        FoodBusiness foodBusiness;

        public void Get()
        {

            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlDataAdapter sda = new SqlDataAdapter("select * from Foods", conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            dataGridViewFood.DataSource = dt;
        }

        
        public Foods()
        {
            InitializeComponent();
            this.foodBusiness = new FoodBusiness();
        }

        private void buttonAddDrink_Click(object sender, EventArgs e)
        {
            string Str1 = textBoxQuantityFood.Text.Trim();
            double Num1;
            bool isNum1 = double.TryParse(Str1, out Num1);

            string Str2 = textBoxPriceFood.Text.Trim();
            int Num2;
            bool isNum2 = int.TryParse(Str2, out Num2);

            if (textBoxFoodName.Text != "" && textBoxQuantityFood.Text != "" && textBoxUnitOfMeasureFood.Text != "" && textBoxPriceFood.Text != "" && textBoxCurrencyFood.Text != "" && comboBoxCategoryFood.Text != "")
            {
                Food f = new Food();
                if (isNum1 && isNum2)
                {
                    f.FoodName = textBoxFoodName.Text;
                    f.Quantity = Convert.ToDouble(textBoxQuantityFood.Text);
                    f.UnitOfMeasure = textBoxUnitOfMeasureFood.Text;
                    f.Price = Convert.ToInt32(textBoxPriceFood.Text);
                    f.Currency = textBoxCurrencyFood.Text;
                    f.CategoryFood = comboBoxCategoryFood.Text;

                    this.foodBusiness.NewFood(f);
                    this.Get();
                }
                else
                {
                    MessageBox.Show("IdJela i cena moraju biti celi brojevi, kolicina moze biti realni broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void buttonDeleteDrink_Click(object sender, EventArgs e)
        {
            string Str = textBoxIdFood.Text.Trim();
            int Num;
            bool isNum = int.TryParse(Str, out Num);

            if (textBoxIdFood.Text != "")
            {
                Food d = new Food();
                if (isNum)
                {
                    d.IdFood = Convert.ToInt32(textBoxIdFood.Text);
                    this.foodBusiness.DeleteFood(d);

                    this.Get();
                }
                else
                {
                    MessageBox.Show("IdPica i cena moraju biti celi brojevi, kolicina moze biti realni broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Morate uneti IdPica ako zelite da obrisete!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void buttonEditDrink_Click(object sender, EventArgs e)
        {
            string Str = textBoxIdFood.Text.Trim();
            int Num;
            bool isNum = int.TryParse(Str, out Num);

            string Str1 = textBoxQuantityFood.Text.Trim();
            double Num1;
            bool isNum1 = double.TryParse(Str1, out Num1);

            string Str2 = textBoxPriceFood.Text.Trim();
            int Num2;
            bool isNum2 = int.TryParse(Str2, out Num2);

            if (textBoxIdFood.Text != "" && textBoxFoodName.Text != "" && textBoxQuantityFood.Text != "" && textBoxUnitOfMeasureFood.Text != "" && textBoxPriceFood.Text != "" && textBoxCurrencyFood.Text != "" && comboBoxCategoryFood.Text != "")
            {
                Food d = new Food();
                if (isNum && isNum1 && isNum2)
                {
                    d.IdFood = Convert.ToInt32(textBoxIdFood.Text);
                    d.FoodName = textBoxFoodName.Text;
                    d.Quantity = Convert.ToDouble(textBoxQuantityFood.Text);
                    d.UnitOfMeasure = textBoxUnitOfMeasureFood.Text;
                    d.Price = Convert.ToInt32(textBoxPriceFood.Text);
                    d.Currency = textBoxQuantityFood.Text;
                    d.CategoryFood = comboBoxCategoryFood.Text;

                    this.foodBusiness.UpdateFood(d);

                    this.Get();
                }
                else
                {
                    {
                        MessageBox.Show("IdPica i cena moraju biti celi brojevi, kolicina moze biti realni broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void Foods_Load(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Get();
        }

        private void buttonFoods_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Show();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Hide();
        }

        private void buttonDrinks_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Show();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Hide();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login l = new Login();
            l.Show();
        }

        private void textBoxFoodName_TextChanged(object sender, EventArgs e)
        {
            if (textBoxFoodName.Text == "")
            {
                label1.Show();
            }
            else
            {
                label1.Hide();
            }
        }

        private void textBoxQuantityFood_TextChanged(object sender, EventArgs e)
        {
            if (textBoxQuantityFood.Text == "")
            {
                label2.Show();
            }
            else
            {
                label2.Hide();
            }
        }

        private void textBoxUnitOfMeasureFood_TextChanged(object sender, EventArgs e)
        {
            if (textBoxUnitOfMeasureFood.Text == "")
            {
                label3.Show();
            }
            else
            {
                label3.Hide();
            }
        }

        private void textBoxPriceFood_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPriceFood.Text == "")
            {
                label4.Show();
            }
            else
            {
                label4.Hide();
            }
        }

        private void textBoxCurrencyFood_TextChanged(object sender, EventArgs e)
        {
            if (textBoxCurrencyFood.Text == "")
            {
                label6.Show();
            }
            else
            {
                label6.Hide();
            }
        }

        private void comboBoxCategoryFood_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCategoryFood.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide();
            }
        }

        private void dataGridViewFood_Click(object sender, EventArgs e)
        {
            textBoxIdFood.Text = this.dataGridViewFood.CurrentRow.Cells[0].Value.ToString();
            textBoxFoodName.Text = this.dataGridViewFood.CurrentRow.Cells[1].Value.ToString();
            textBoxQuantityFood.Text = this.dataGridViewFood.CurrentRow.Cells[2].Value.ToString();
            textBoxUnitOfMeasureFood.Text = this.dataGridViewFood.CurrentRow.Cells[3].Value.ToString();
            textBoxPriceFood.Text = this.dataGridViewFood.CurrentRow.Cells[4].Value.ToString();
            textBoxCurrencyFood.Text = this.dataGridViewFood.CurrentRow.Cells[5].Value.ToString();
            comboBoxCategoryFood.Text = this.dataGridViewFood.CurrentRow.Cells[6].Value.ToString();
        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Gainsboro;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Khaki;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Show();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Hide();
        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Show();
            OrderItems i = new OrderItems();
            i.Hide();
        }

        private void buttonOrderItems_Click(object sender, EventArgs e)
        {
            buttonDrinks.BackColor = Color.Gainsboro;
            buttonFoods.BackColor = Color.Khaki;
            buttonOrders.BackColor = Color.Gainsboro;
            buttonOrderItems.BackColor = Color.Gainsboro;
            buttonUsers.BackColor = Color.Gainsboro;

            this.Hide();
            Settings drink = new Settings();
            drink.Hide();
            Users u = new Users();
            u.Hide();
            Orders o = new Orders();
            o.Hide();
            OrderItems i = new OrderItems();
            i.Show();
        }
    }
}
