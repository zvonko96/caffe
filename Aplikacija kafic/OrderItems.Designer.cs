﻿namespace Aplikacija_kafic
{
    partial class OrderItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderItems));
            this.panelDrinks = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelIdItem = new System.Windows.Forms.Label();
            this.textBoxIdOrderItems = new System.Windows.Forms.TextBox();
            this.labelIdOrder = new System.Windows.Forms.Label();
            this.labelIdFood = new System.Windows.Forms.Label();
            this.labelIdDrink = new System.Windows.Forms.Label();
            this.textBoxIdDrinks = new System.Windows.Forms.TextBox();
            this.textBoxIdFood = new System.Windows.Forms.TextBox();
            this.textBoxIdOrders = new System.Windows.Forms.TextBox();
            this.pictureBoxDrink = new System.Windows.Forms.PictureBox();
            this.buttonEditFood = new System.Windows.Forms.Button();
            this.buttonDeleteFood = new System.Windows.Forms.Button();
            this.buttonAddFood = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.buttonDrinks = new System.Windows.Forms.Button();
            this.buttonUsers = new System.Windows.Forms.Button();
            this.buttonOrderItems = new System.Windows.Forms.Button();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonFoods = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.dataGridViewOrderItems = new System.Windows.Forms.DataGridView();
            this.IdOrderItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDrinkIt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdFoodIt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdOrderIt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelDrinks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).BeginInit();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrderItems)).BeginInit();
            this.SuspendLayout();
            // 
            // panelDrinks
            // 
            this.panelDrinks.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelDrinks.Controls.Add(this.dataGridViewOrderItems);
            this.panelDrinks.Controls.Add(this.label3);
            this.panelDrinks.Controls.Add(this.label2);
            this.panelDrinks.Controls.Add(this.label1);
            this.panelDrinks.Controls.Add(this.labelIdItem);
            this.panelDrinks.Controls.Add(this.textBoxIdOrderItems);
            this.panelDrinks.Controls.Add(this.labelIdOrder);
            this.panelDrinks.Controls.Add(this.labelIdFood);
            this.panelDrinks.Controls.Add(this.labelIdDrink);
            this.panelDrinks.Controls.Add(this.textBoxIdDrinks);
            this.panelDrinks.Controls.Add(this.textBoxIdFood);
            this.panelDrinks.Controls.Add(this.textBoxIdOrders);
            this.panelDrinks.Controls.Add(this.pictureBoxDrink);
            this.panelDrinks.Controls.Add(this.buttonEditFood);
            this.panelDrinks.Controls.Add(this.buttonDeleteFood);
            this.panelDrinks.Controls.Add(this.buttonAddFood);
            this.panelDrinks.Location = new System.Drawing.Point(193, 34);
            this.panelDrinks.Name = "panelDrinks";
            this.panelDrinks.Size = new System.Drawing.Size(1173, 731);
            this.panelDrinks.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(493, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 26);
            this.label3.TabIndex = 27;
            this.label3.Text = "*";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(493, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 26);
            this.label2.TabIndex = 26;
            this.label2.Text = "*";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(493, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 26);
            this.label1.TabIndex = 25;
            this.label1.Text = "*";
            // 
            // labelIdItem
            // 
            this.labelIdItem.AutoSize = true;
            this.labelIdItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdItem.Location = new System.Drawing.Point(73, 74);
            this.labelIdItem.Name = "labelIdItem";
            this.labelIdItem.Size = new System.Drawing.Size(85, 18);
            this.labelIdItem.TabIndex = 18;
            this.labelIdItem.Text = "Id stavke: ";
            this.labelIdItem.Visible = false;
            // 
            // textBoxIdOrderItems
            // 
            this.textBoxIdOrderItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBoxIdOrderItems.Location = new System.Drawing.Point(202, 71);
            this.textBoxIdOrderItems.Name = "textBoxIdOrderItems";
            this.textBoxIdOrderItems.Size = new System.Drawing.Size(285, 24);
            this.textBoxIdOrderItems.TabIndex = 17;
            this.textBoxIdOrderItems.Visible = false;
            // 
            // labelIdOrder
            // 
            this.labelIdOrder.AutoSize = true;
            this.labelIdOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdOrder.Location = new System.Drawing.Point(73, 226);
            this.labelIdOrder.Name = "labelIdOrder";
            this.labelIdOrder.Size = new System.Drawing.Size(118, 18);
            this.labelIdOrder.TabIndex = 14;
            this.labelIdOrder.Text = "Id narudzbine: ";
            // 
            // labelIdFood
            // 
            this.labelIdFood.AutoSize = true;
            this.labelIdFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdFood.Location = new System.Drawing.Point(73, 174);
            this.labelIdFood.Name = "labelIdFood";
            this.labelIdFood.Size = new System.Drawing.Size(78, 18);
            this.labelIdFood.TabIndex = 13;
            this.labelIdFood.Text = "Id hrane: ";
            // 
            // labelIdDrink
            // 
            this.labelIdDrink.AutoSize = true;
            this.labelIdDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdDrink.Location = new System.Drawing.Point(73, 122);
            this.labelIdDrink.Name = "labelIdDrink";
            this.labelIdDrink.Size = new System.Drawing.Size(67, 18);
            this.labelIdDrink.TabIndex = 12;
            this.labelIdDrink.Text = "Id pica: ";
            // 
            // textBoxIdDrinks
            // 
            this.textBoxIdDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIdDrinks.Location = new System.Drawing.Point(202, 119);
            this.textBoxIdDrinks.Name = "textBoxIdDrinks";
            this.textBoxIdDrinks.Size = new System.Drawing.Size(285, 24);
            this.textBoxIdDrinks.TabIndex = 1;
            this.textBoxIdDrinks.TextChanged += new System.EventHandler(this.textBoxIdDrinks_TextChanged);
            // 
            // textBoxIdFood
            // 
            this.textBoxIdFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIdFood.Location = new System.Drawing.Point(202, 171);
            this.textBoxIdFood.Name = "textBoxIdFood";
            this.textBoxIdFood.Size = new System.Drawing.Size(285, 24);
            this.textBoxIdFood.TabIndex = 2;
            this.textBoxIdFood.TextChanged += new System.EventHandler(this.textBoxIdFood_TextChanged);
            // 
            // textBoxIdOrders
            // 
            this.textBoxIdOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIdOrders.Location = new System.Drawing.Point(202, 223);
            this.textBoxIdOrders.Name = "textBoxIdOrders";
            this.textBoxIdOrders.Size = new System.Drawing.Size(285, 24);
            this.textBoxIdOrders.TabIndex = 3;
            this.textBoxIdOrders.TextChanged += new System.EventHandler(this.textBoxIdOrders_TextChanged);
            // 
            // pictureBoxDrink
            // 
            this.pictureBoxDrink.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDrink.BackgroundImage")));
            this.pictureBoxDrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDrink.Location = new System.Drawing.Point(548, 102);
            this.pictureBoxDrink.Name = "pictureBoxDrink";
            this.pictureBoxDrink.Size = new System.Drawing.Size(587, 259);
            this.pictureBoxDrink.TabIndex = 11;
            this.pictureBoxDrink.TabStop = false;
            // 
            // buttonEditFood
            // 
            this.buttonEditFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditFood.Location = new System.Drawing.Point(400, 277);
            this.buttonEditFood.Name = "buttonEditFood";
            this.buttonEditFood.Size = new System.Drawing.Size(87, 36);
            this.buttonEditFood.TabIndex = 8;
            this.buttonEditFood.Text = "Izmeni";
            this.buttonEditFood.UseVisualStyleBackColor = true;
            this.buttonEditFood.Click += new System.EventHandler(this.buttonEditFood_Click);
            // 
            // buttonDeleteFood
            // 
            this.buttonDeleteFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteFood.Location = new System.Drawing.Point(304, 277);
            this.buttonDeleteFood.Name = "buttonDeleteFood";
            this.buttonDeleteFood.Size = new System.Drawing.Size(90, 36);
            this.buttonDeleteFood.TabIndex = 7;
            this.buttonDeleteFood.Text = "Obrisi";
            this.buttonDeleteFood.UseVisualStyleBackColor = true;
            this.buttonDeleteFood.Click += new System.EventHandler(this.buttonDeleteFood_Click);
            // 
            // buttonAddFood
            // 
            this.buttonAddFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddFood.Location = new System.Drawing.Point(202, 277);
            this.buttonAddFood.Name = "buttonAddFood";
            this.buttonAddFood.Size = new System.Drawing.Size(88, 36);
            this.buttonAddFood.TabIndex = 6;
            this.buttonAddFood.Text = "Dodaj";
            this.buttonAddFood.UseVisualStyleBackColor = true;
            this.buttonAddFood.Click += new System.EventHandler(this.buttonAddFood_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1290, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 28);
            this.button1.TabIndex = 14;
            this.button1.Text = "_";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogOut.Location = new System.Drawing.Point(5, 705);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(180, 52);
            this.buttonLogOut.TabIndex = 7;
            this.buttonLogOut.Text = "Log out";
            this.buttonLogOut.UseVisualStyleBackColor = true;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // buttonDrinks
            // 
            this.buttonDrinks.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonDrinks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDrinks.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonDrinks.Location = new System.Drawing.Point(5, 3);
            this.buttonDrinks.Name = "buttonDrinks";
            this.buttonDrinks.Size = new System.Drawing.Size(180, 52);
            this.buttonDrinks.TabIndex = 0;
            this.buttonDrinks.Text = "Pica";
            this.buttonDrinks.UseVisualStyleBackColor = false;
            this.buttonDrinks.Click += new System.EventHandler(this.buttonDrinks_Click);
            // 
            // buttonUsers
            // 
            this.buttonUsers.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUsers.Location = new System.Drawing.Point(5, 211);
            this.buttonUsers.Name = "buttonUsers";
            this.buttonUsers.Size = new System.Drawing.Size(180, 52);
            this.buttonUsers.TabIndex = 4;
            this.buttonUsers.Text = "Korisnici";
            this.buttonUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonUsers.UseVisualStyleBackColor = false;
            this.buttonUsers.Click += new System.EventHandler(this.buttonUsers_Click);
            // 
            // buttonOrderItems
            // 
            this.buttonOrderItems.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrderItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrderItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrderItems.Location = new System.Drawing.Point(5, 159);
            this.buttonOrderItems.Name = "buttonOrderItems";
            this.buttonOrderItems.Size = new System.Drawing.Size(180, 52);
            this.buttonOrderItems.TabIndex = 3;
            this.buttonOrderItems.Text = "Stavke narudzbine";
            this.buttonOrderItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOrderItems.UseVisualStyleBackColor = false;
            this.buttonOrderItems.Click += new System.EventHandler(this.buttonOrderItems_Click);
            // 
            // buttonOrders
            // 
            this.buttonOrders.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrders.Location = new System.Drawing.Point(5, 107);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Size = new System.Drawing.Size(180, 52);
            this.buttonOrders.TabIndex = 2;
            this.buttonOrders.Text = "Narudzbine";
            this.buttonOrders.UseVisualStyleBackColor = false;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonFoods
            // 
            this.buttonFoods.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonFoods.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFoods.Location = new System.Drawing.Point(5, 55);
            this.buttonFoods.Name = "buttonFoods";
            this.buttonFoods.Size = new System.Drawing.Size(180, 52);
            this.buttonFoods.TabIndex = 1;
            this.buttonFoods.Text = "Jela";
            this.buttonFoods.UseVisualStyleBackColor = false;
            this.buttonFoods.Click += new System.EventHandler(this.buttonFoods_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(1326, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(38, 28);
            this.buttonClose.TabIndex = 13;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelMenu.Controls.Add(this.buttonLogOut);
            this.panelMenu.Controls.Add(this.buttonUsers);
            this.panelMenu.Controls.Add(this.buttonOrderItems);
            this.panelMenu.Controls.Add(this.buttonOrders);
            this.panelMenu.Controls.Add(this.buttonFoods);
            this.panelMenu.Controls.Add(this.buttonDrinks);
            this.panelMenu.Location = new System.Drawing.Point(0, -2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(192, 772);
            this.panelMenu.TabIndex = 11;
            // 
            // dataGridViewOrderItems
            // 
            this.dataGridViewOrderItems.AllowUserToOrderColumns = true;
            this.dataGridViewOrderItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrderItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdOrderItem,
            this.IdDrinkIt,
            this.IdFoodIt,
            this.IdOrderIt});
            this.dataGridViewOrderItems.Location = new System.Drawing.Point(1, 485);
            this.dataGridViewOrderItems.Name = "dataGridViewOrderItems";
            this.dataGridViewOrderItems.Size = new System.Drawing.Size(1170, 251);
            this.dataGridViewOrderItems.TabIndex = 28;
            this.dataGridViewOrderItems.Click += new System.EventHandler(this.dataGridViewOrderItems_Click_1);
            // 
            // IdOrderItem
            // 
            this.IdOrderItem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IdOrderItem.DataPropertyName = "IdItem";
            this.IdOrderItem.HeaderText = "IdStavke";
            this.IdOrderItem.Name = "IdOrderItem";
            // 
            // IdDrinkIt
            // 
            this.IdDrinkIt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IdDrinkIt.DataPropertyName = " IdDrinkIt";
            this.IdDrinkIt.HeaderText = "IdPica";
            this.IdDrinkIt.Name = "IdDrinkIt";
            // 
            // IdFoodIt
            // 
            this.IdFoodIt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IdFoodIt.DataPropertyName = "IdFoodIt";
            this.IdFoodIt.HeaderText = "IdJela";
            this.IdFoodIt.Name = "IdFoodIt";
            // 
            // IdOrderIt
            // 
            this.IdOrderIt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IdOrderIt.DataPropertyName = "IdOrderIt";
            this.IdOrderIt.HeaderText = "IdNarudzbine";
            this.IdOrderIt.Name = "IdOrderIt";
            // 
            // OrderItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.panelDrinks);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.panelMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "OrderItems";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderItems";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrderItems_Load);
            this.panelDrinks.ResumeLayout(false);
            this.panelDrinks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).EndInit();
            this.panelMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrderItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelDrinks;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelIdItem;
        private System.Windows.Forms.TextBox textBoxIdOrderItems;
        private System.Windows.Forms.Label labelIdOrder;
        private System.Windows.Forms.Label labelIdFood;
        private System.Windows.Forms.Label labelIdDrink;
        private System.Windows.Forms.TextBox textBoxIdDrinks;
        private System.Windows.Forms.TextBox textBoxIdFood;
        private System.Windows.Forms.TextBox textBoxIdOrders;
        private System.Windows.Forms.PictureBox pictureBoxDrink;
        private System.Windows.Forms.Button buttonEditFood;
        private System.Windows.Forms.Button buttonDeleteFood;
        private System.Windows.Forms.Button buttonAddFood;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.Button buttonDrinks;
        private System.Windows.Forms.Button buttonUsers;
        private System.Windows.Forms.Button buttonOrderItems;
        private System.Windows.Forms.Button buttonOrders;
        private System.Windows.Forms.Button buttonFoods;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.DataGridView dataGridViewOrderItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdOrderItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDrinkIt;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdFoodIt;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdOrderIt;
    }
}