﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    public partial class instagramLogin : Form
    {
        public instagramLogin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxInstagramUsername.Text != "" && textBoxInstagramPassword.Text != "")
            {
                SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlDataAdapter sda = new SqlDataAdapter("select isAdmin from [User] where UserNameInstagram='" + textBoxInstagramUsername.Text + "' and PasswordInstagram='" + textBoxInstagramPassword.Text + "'", conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count == 1)
            {

                if (dt.Rows[0][0].ToString() == "True")
                {
                    this.Hide();
                    Settings mm = new Settings();
                    mm.Show();
                }
                else
                {
                    this.Hide();
                    Waiter wt = new Waiter();
                    wt.Show();
                }

            }
            else
            {
                MessageBox.Show("Pogresno ste uneli korisnicko ime ili lozinku!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                label1.Show();
            }
        }

        private void textBoxInstagramUsername_TextChanged(object sender, EventArgs e)
        {
            if (textBoxInstagramUsername.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide();
            }
        }

        private void textBoxInstagramPassword_TextChanged(object sender, EventArgs e)
        {
            if (textBoxInstagramPassword.Text == "")
            {
                label1.Show();
            }
            else
            {
                label1.Hide();
            }
        }
        bool durum;
        private void pictureBoxEye_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxInstagramPassword.UseSystemPasswordChar = false;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxInstagramPassword.UseSystemPasswordChar = true;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }
    }
}
