﻿using BL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    
    public partial class RecoverPassword : Form
    {
        UserBusiness userBusiness;
        public RecoverPassword()
        {
            InitializeComponent();
            this.userBusiness = new UserBusiness();
        }

       

        private void textBoxUserName_TextChanged(object sender, EventArgs e)
        {
            if (textBoxUserName.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide();
            }
        }

        private void textBoxShoes_TextChanged(object sender, EventArgs e)
        {
            if (textBoxShoes.Text == "")
            {
                label1.Show();
            }
            else
            {
                label1.Hide();
            }
        }

        private void textBoxFavoriteCity_TextChanged(object sender, EventArgs e)
        {
            if (textBoxFavoriteCity.Text == "")
            {
                label2.Show();
            }
            else
            {
                label2.Hide();
            }
        }

        private void textBoxFavoriteFood_TextChanged(object sender, EventArgs e)
        {
            if (textBoxFavoriteFood.Text == "")
            {
                label3.Show();
            }
            else
            {
                label3.Hide();
            }
        }

        private void buttonRecover_Click(object sender, EventArgs e)
        {
            string Str = textBoxShoes.Text.Trim();
            double Num;
            bool isNum = double.TryParse(Str, out Num);

            if (textBoxUserName.Text != "" && textBoxShoes.Text != "" && textBoxFavoriteCity.Text != "" && textBoxFavoriteFood.Text != "")
            {
                if (isNum)
                {
                    SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                    SqlDataAdapter sda = new SqlDataAdapter("select * from [User] where UserName='" + textBoxUserName.Text + "' and NumberOfShoes='" + textBoxShoes.Text + "' and FavoriteCity='" + textBoxFavoriteCity.Text + "' and FavoriteFood='" + textBoxFavoriteFood.Text + "'", conn);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    
                    if (dt.Rows.Count == 1)
                    {
                        panel1.Visible = true;
                        textBoxUserName.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Pogresno ste uneli podatke!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("U polje broj patika morate uneti broj!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti polja oznacena zvezdicom!!!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxUserName.Text != "" && textBoxChangePassword.Text != "" && textBoxChangePasswordConfirm.Text != "")
            {
                User u = new User();
                u.UserName = textBoxUserName.Text;
                u.PasswordUserName = textBoxChangePassword.Text;
                u.PasswordUserName = textBoxChangePasswordConfirm.Text;
                if (textBoxChangePassword.Text == textBoxChangePasswordConfirm.Text)
                {
                    label7.Text = this.userBusiness.UpdatePassword(u);
                }
                else
                {
                    MessageBox.Show("Sifre se ne poklapaju!!!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja!!!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        bool durum;
        private void pictureBoxEye_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxShoes.UseSystemPasswordChar = false;
                pictureBoxEyeShoes.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxShoes.UseSystemPasswordChar = true;
                pictureBoxEyeShoes.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void pictureBoxCity_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxFavoriteCity.UseSystemPasswordChar = false;
                pictureBoxCity.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxFavoriteCity.UseSystemPasswordChar = true;
                pictureBoxCity.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void pictureBoxFood_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxFavoriteFood.UseSystemPasswordChar = false;
                pictureBoxFood.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxFavoriteFood.UseSystemPasswordChar = true;
                pictureBoxFood.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxChangePassword.UseSystemPasswordChar = false;
                pictureBoxChangePicture.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxChangePassword.UseSystemPasswordChar = true;
                pictureBoxChangePicture.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void pictureBoxConfirmPassword_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxChangePasswordConfirm.UseSystemPasswordChar = false;
                pictureBoxConfirmPassword.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxChangePasswordConfirm.UseSystemPasswordChar = true;
                pictureBoxConfirmPassword.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }
    }
}
