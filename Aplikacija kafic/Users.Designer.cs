﻿namespace Aplikacija_kafic
{
    partial class Users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Users));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.buttonUsers = new System.Windows.Forms.Button();
            this.buttonOrderItems = new System.Windows.Forms.Button();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonFoods = new System.Windows.Forms.Button();
            this.buttonDrinks = new System.Windows.Forms.Button();
            this.panelDrinks = new System.Windows.Forms.Panel();
            this.checkBoxIsAdmin = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.labelFavoriteFood = new System.Windows.Forms.Label();
            this.textBoxFavoriteFood = new System.Windows.Forms.TextBox();
            this.textBoxFavoriteCity = new System.Windows.Forms.TextBox();
            this.labelFavoriteCity = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelNumberOfShoes = new System.Windows.Forms.Label();
            this.textBoxNumberOfShoes = new System.Windows.Forms.TextBox();
            this.textBoxPasswordInstagram = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelUsernameInstagram = new System.Windows.Forms.Label();
            this.textBoxUserNameInstagram = new System.Windows.Forms.TextBox();
            this.textBoxPasswordEmail = new System.Windows.Forms.TextBox();
            this.labelPasswordEmail = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.textBoxFacebookPassword = new System.Windows.Forms.TextBox();
            this.labelPasswordFacebook = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelIdUser = new System.Windows.Forms.Label();
            this.textBoxIdUser = new System.Windows.Forms.TextBox();
            this.labelFacebookEmail = new System.Windows.Forms.Label();
            this.labelPasswordUsername = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxNameUser = new System.Windows.Forms.TextBox();
            this.textBoxSurnameUser = new System.Windows.Forms.TextBox();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.dataGridViewUser = new System.Windows.Forms.DataGridView();
            this.intIdJela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNazivJela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtJedinicaMere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.intPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValuta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbKategorija = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxPasswordUserName = new System.Windows.Forms.TextBox();
            this.pictureBoxDrink = new System.Windows.Forms.PictureBox();
            this.buttonEditUser = new System.Windows.Forms.Button();
            this.buttonDeleteUser = new System.Windows.Forms.Button();
            this.buttonAddUser = new System.Windows.Forms.Button();
            this.textBoxFacebookEmail = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panelMenu.SuspendLayout();
            this.panelDrinks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelMenu.Controls.Add(this.buttonLogOut);
            this.panelMenu.Controls.Add(this.buttonUsers);
            this.panelMenu.Controls.Add(this.buttonOrderItems);
            this.panelMenu.Controls.Add(this.buttonOrders);
            this.panelMenu.Controls.Add(this.buttonFoods);
            this.panelMenu.Controls.Add(this.buttonDrinks);
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(192, 772);
            this.panelMenu.TabIndex = 8;
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogOut.Location = new System.Drawing.Point(5, 705);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(180, 52);
            this.buttonLogOut.TabIndex = 7;
            this.buttonLogOut.Text = "Log out";
            this.buttonLogOut.UseVisualStyleBackColor = true;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // buttonUsers
            // 
            this.buttonUsers.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUsers.Location = new System.Drawing.Point(5, 211);
            this.buttonUsers.Name = "buttonUsers";
            this.buttonUsers.Size = new System.Drawing.Size(180, 52);
            this.buttonUsers.TabIndex = 4;
            this.buttonUsers.Text = "Korisnici";
            this.buttonUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonUsers.UseVisualStyleBackColor = false;
            this.buttonUsers.Click += new System.EventHandler(this.buttonUsers_Click);
            // 
            // buttonOrderItems
            // 
            this.buttonOrderItems.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrderItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrderItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrderItems.Location = new System.Drawing.Point(5, 159);
            this.buttonOrderItems.Name = "buttonOrderItems";
            this.buttonOrderItems.Size = new System.Drawing.Size(180, 52);
            this.buttonOrderItems.TabIndex = 3;
            this.buttonOrderItems.Text = "Stavke narudzbine";
            this.buttonOrderItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOrderItems.UseVisualStyleBackColor = false;
            this.buttonOrderItems.Click += new System.EventHandler(this.buttonOrderItems_Click);
            // 
            // buttonOrders
            // 
            this.buttonOrders.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrders.Location = new System.Drawing.Point(5, 107);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Size = new System.Drawing.Size(180, 52);
            this.buttonOrders.TabIndex = 2;
            this.buttonOrders.Text = "Narudzbine";
            this.buttonOrders.UseVisualStyleBackColor = false;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonFoods
            // 
            this.buttonFoods.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonFoods.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFoods.Location = new System.Drawing.Point(5, 55);
            this.buttonFoods.Name = "buttonFoods";
            this.buttonFoods.Size = new System.Drawing.Size(180, 52);
            this.buttonFoods.TabIndex = 1;
            this.buttonFoods.Text = "Jela";
            this.buttonFoods.UseVisualStyleBackColor = false;
            this.buttonFoods.Click += new System.EventHandler(this.buttonFoods_Click);
            // 
            // buttonDrinks
            // 
            this.buttonDrinks.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonDrinks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDrinks.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonDrinks.Location = new System.Drawing.Point(5, 3);
            this.buttonDrinks.Name = "buttonDrinks";
            this.buttonDrinks.Size = new System.Drawing.Size(180, 52);
            this.buttonDrinks.TabIndex = 0;
            this.buttonDrinks.Text = "Pica";
            this.buttonDrinks.UseVisualStyleBackColor = false;
            this.buttonDrinks.Click += new System.EventHandler(this.buttonDrinks_Click);
            // 
            // panelDrinks
            // 
            this.panelDrinks.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelDrinks.Controls.Add(this.checkBoxIsAdmin);
            this.panelDrinks.Controls.Add(this.label18);
            this.panelDrinks.Controls.Add(this.labelFavoriteFood);
            this.panelDrinks.Controls.Add(this.textBoxFavoriteFood);
            this.panelDrinks.Controls.Add(this.textBoxFavoriteCity);
            this.panelDrinks.Controls.Add(this.labelFavoriteCity);
            this.panelDrinks.Controls.Add(this.label10);
            this.panelDrinks.Controls.Add(this.label14);
            this.panelDrinks.Controls.Add(this.labelNumberOfShoes);
            this.panelDrinks.Controls.Add(this.textBoxNumberOfShoes);
            this.panelDrinks.Controls.Add(this.textBoxPasswordInstagram);
            this.panelDrinks.Controls.Add(this.label11);
            this.panelDrinks.Controls.Add(this.label12);
            this.panelDrinks.Controls.Add(this.label13);
            this.panelDrinks.Controls.Add(this.labelUsernameInstagram);
            this.panelDrinks.Controls.Add(this.textBoxUserNameInstagram);
            this.panelDrinks.Controls.Add(this.textBoxPasswordEmail);
            this.panelDrinks.Controls.Add(this.labelPasswordEmail);
            this.panelDrinks.Controls.Add(this.label8);
            this.panelDrinks.Controls.Add(this.label9);
            this.panelDrinks.Controls.Add(this.labelEmail);
            this.panelDrinks.Controls.Add(this.textBoxEmail);
            this.panelDrinks.Controls.Add(this.textBoxFacebookPassword);
            this.panelDrinks.Controls.Add(this.labelPasswordFacebook);
            this.panelDrinks.Controls.Add(this.label5);
            this.panelDrinks.Controls.Add(this.label6);
            this.panelDrinks.Controls.Add(this.label4);
            this.panelDrinks.Controls.Add(this.label3);
            this.panelDrinks.Controls.Add(this.label2);
            this.panelDrinks.Controls.Add(this.label1);
            this.panelDrinks.Controls.Add(this.labelIdUser);
            this.panelDrinks.Controls.Add(this.textBoxIdUser);
            this.panelDrinks.Controls.Add(this.labelFacebookEmail);
            this.panelDrinks.Controls.Add(this.labelPasswordUsername);
            this.panelDrinks.Controls.Add(this.labelUserName);
            this.panelDrinks.Controls.Add(this.labelSurname);
            this.panelDrinks.Controls.Add(this.labelName);
            this.panelDrinks.Controls.Add(this.textBoxNameUser);
            this.panelDrinks.Controls.Add(this.textBoxSurnameUser);
            this.panelDrinks.Controls.Add(this.textBoxUserName);
            this.panelDrinks.Controls.Add(this.dataGridViewUser);
            this.panelDrinks.Controls.Add(this.textBoxPasswordUserName);
            this.panelDrinks.Controls.Add(this.pictureBoxDrink);
            this.panelDrinks.Controls.Add(this.buttonEditUser);
            this.panelDrinks.Controls.Add(this.buttonDeleteUser);
            this.panelDrinks.Controls.Add(this.buttonAddUser);
            this.panelDrinks.Controls.Add(this.textBoxFacebookEmail);
            this.panelDrinks.Location = new System.Drawing.Point(193, 36);
            this.panelDrinks.Name = "panelDrinks";
            this.panelDrinks.Size = new System.Drawing.Size(1173, 731);
            this.panelDrinks.TabIndex = 9;
            // 
            // checkBoxIsAdmin
            // 
            this.checkBoxIsAdmin.AutoSize = true;
            this.checkBoxIsAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.checkBoxIsAdmin.Location = new System.Drawing.Point(520, 430);
            this.checkBoxIsAdmin.Name = "checkBoxIsAdmin";
            this.checkBoxIsAdmin.Size = new System.Drawing.Size(70, 20);
            this.checkBoxIsAdmin.TabIndex = 57;
            this.checkBoxIsAdmin.Text = "Adim?";
            this.checkBoxIsAdmin.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Maroon;
            this.label18.Location = new System.Drawing.Point(493, 386);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 26);
            this.label18.TabIndex = 54;
            this.label18.Text = "*";
            // 
            // labelFavoriteFood
            // 
            this.labelFavoriteFood.AutoSize = true;
            this.labelFavoriteFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelFavoriteFood.Location = new System.Drawing.Point(13, 386);
            this.labelFavoriteFood.Name = "labelFavoriteFood";
            this.labelFavoriteFood.Size = new System.Drawing.Size(115, 16);
            this.labelFavoriteFood.TabIndex = 53;
            this.labelFavoriteFood.Text = "OmiljenaHrana ";
            // 
            // textBoxFavoriteFood
            // 
            this.textBoxFavoriteFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxFavoriteFood.Location = new System.Drawing.Point(202, 383);
            this.textBoxFavoriteFood.Name = "textBoxFavoriteFood";
            this.textBoxFavoriteFood.Size = new System.Drawing.Size(285, 22);
            this.textBoxFavoriteFood.TabIndex = 52;
            // 
            // textBoxFavoriteCity
            // 
            this.textBoxFavoriteCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxFavoriteCity.Location = new System.Drawing.Point(202, 355);
            this.textBoxFavoriteCity.Name = "textBoxFavoriteCity";
            this.textBoxFavoriteCity.Size = new System.Drawing.Size(285, 22);
            this.textBoxFavoriteCity.TabIndex = 51;
            // 
            // labelFavoriteCity
            // 
            this.labelFavoriteCity.AutoSize = true;
            this.labelFavoriteCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelFavoriteCity.Location = new System.Drawing.Point(13, 358);
            this.labelFavoriteCity.Name = "labelFavoriteCity";
            this.labelFavoriteCity.Size = new System.Drawing.Size(108, 16);
            this.labelFavoriteCity.TabIndex = 50;
            this.labelFavoriteCity.Text = "Omiljeni grad: ";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(493, 358);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 26);
            this.label10.TabIndex = 49;
            this.label10.Text = "*";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(493, 330);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 26);
            this.label14.TabIndex = 48;
            this.label14.Text = "*";
            // 
            // labelNumberOfShoes
            // 
            this.labelNumberOfShoes.AutoSize = true;
            this.labelNumberOfShoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelNumberOfShoes.Location = new System.Drawing.Point(13, 330);
            this.labelNumberOfShoes.Name = "labelNumberOfShoes";
            this.labelNumberOfShoes.Size = new System.Drawing.Size(91, 16);
            this.labelNumberOfShoes.TabIndex = 47;
            this.labelNumberOfShoes.Text = "Broj patika: ";
            // 
            // textBoxNumberOfShoes
            // 
            this.textBoxNumberOfShoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxNumberOfShoes.Location = new System.Drawing.Point(202, 327);
            this.textBoxNumberOfShoes.Name = "textBoxNumberOfShoes";
            this.textBoxNumberOfShoes.Size = new System.Drawing.Size(285, 22);
            this.textBoxNumberOfShoes.TabIndex = 46;
            // 
            // textBoxPasswordInstagram
            // 
            this.textBoxPasswordInstagram.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxPasswordInstagram.Location = new System.Drawing.Point(202, 299);
            this.textBoxPasswordInstagram.Name = "textBoxPasswordInstagram";
            this.textBoxPasswordInstagram.Size = new System.Drawing.Size(285, 22);
            this.textBoxPasswordInstagram.TabIndex = 45;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(13, 302);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 16);
            this.label11.TabIndex = 44;
            this.label11.Text = "Sifra instagram-a: ";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(493, 302);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 26);
            this.label12.TabIndex = 43;
            this.label12.Text = "*";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(493, 274);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 26);
            this.label13.TabIndex = 42;
            this.label13.Text = "*";
            // 
            // labelUsernameInstagram
            // 
            this.labelUsernameInstagram.AutoSize = true;
            this.labelUsernameInstagram.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelUsernameInstagram.Location = new System.Drawing.Point(13, 274);
            this.labelUsernameInstagram.Name = "labelUsernameInstagram";
            this.labelUsernameInstagram.Size = new System.Drawing.Size(189, 16);
            this.labelUsernameInstagram.TabIndex = 41;
            this.labelUsernameInstagram.Text = "Korisnicko ime instagram: ";
            // 
            // textBoxUserNameInstagram
            // 
            this.textBoxUserNameInstagram.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxUserNameInstagram.Location = new System.Drawing.Point(202, 271);
            this.textBoxUserNameInstagram.Name = "textBoxUserNameInstagram";
            this.textBoxUserNameInstagram.Size = new System.Drawing.Size(285, 22);
            this.textBoxUserNameInstagram.TabIndex = 40;
            // 
            // textBoxPasswordEmail
            // 
            this.textBoxPasswordEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxPasswordEmail.Location = new System.Drawing.Point(202, 243);
            this.textBoxPasswordEmail.Name = "textBoxPasswordEmail";
            this.textBoxPasswordEmail.Size = new System.Drawing.Size(285, 22);
            this.textBoxPasswordEmail.TabIndex = 39;
            // 
            // labelPasswordEmail
            // 
            this.labelPasswordEmail.AutoSize = true;
            this.labelPasswordEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelPasswordEmail.Location = new System.Drawing.Point(13, 246);
            this.labelPasswordEmail.Name = "labelPasswordEmail";
            this.labelPasswordEmail.Size = new System.Drawing.Size(104, 16);
            this.labelPasswordEmail.TabIndex = 38;
            this.labelPasswordEmail.Text = "Sifra email-a: ";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(493, 246);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 26);
            this.label8.TabIndex = 37;
            this.label8.Text = "*";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(493, 218);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 26);
            this.label9.TabIndex = 36;
            this.label9.Text = "*";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelEmail.Location = new System.Drawing.Point(13, 218);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(55, 16);
            this.labelEmail.TabIndex = 35;
            this.labelEmail.Text = "Email: ";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxEmail.Location = new System.Drawing.Point(202, 215);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(285, 22);
            this.textBoxEmail.TabIndex = 34;
            // 
            // textBoxFacebookPassword
            // 
            this.textBoxFacebookPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxFacebookPassword.Location = new System.Drawing.Point(202, 187);
            this.textBoxFacebookPassword.Name = "textBoxFacebookPassword";
            this.textBoxFacebookPassword.Size = new System.Drawing.Size(285, 22);
            this.textBoxFacebookPassword.TabIndex = 33;
            // 
            // labelPasswordFacebook
            // 
            this.labelPasswordFacebook.AutoSize = true;
            this.labelPasswordFacebook.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelPasswordFacebook.Location = new System.Drawing.Point(13, 190);
            this.labelPasswordFacebook.Name = "labelPasswordFacebook";
            this.labelPasswordFacebook.Size = new System.Drawing.Size(131, 16);
            this.labelPasswordFacebook.TabIndex = 32;
            this.labelPasswordFacebook.Text = "Sifra facebook-a: ";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(493, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 26);
            this.label5.TabIndex = 31;
            this.label5.Text = "*";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(493, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 26);
            this.label6.TabIndex = 29;
            this.label6.Text = "*";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(493, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 26);
            this.label4.TabIndex = 28;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(493, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 26);
            this.label3.TabIndex = 27;
            this.label3.Text = "*";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(493, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 26);
            this.label2.TabIndex = 26;
            this.label2.Text = "*";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(493, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 26);
            this.label1.TabIndex = 25;
            this.label1.Text = "*";
            // 
            // labelIdUser
            // 
            this.labelIdUser.AutoSize = true;
            this.labelIdUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdUser.Location = new System.Drawing.Point(13, 22);
            this.labelIdUser.Name = "labelIdUser";
            this.labelIdUser.Size = new System.Drawing.Size(96, 16);
            this.labelIdUser.TabIndex = 18;
            this.labelIdUser.Text = "Id korisnika: ";
            this.labelIdUser.Visible = false;
            // 
            // textBoxIdUser
            // 
            this.textBoxIdUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIdUser.Location = new System.Drawing.Point(202, 19);
            this.textBoxIdUser.Name = "textBoxIdUser";
            this.textBoxIdUser.Size = new System.Drawing.Size(285, 22);
            this.textBoxIdUser.TabIndex = 17;
            this.textBoxIdUser.Visible = false;
            // 
            // labelFacebookEmail
            // 
            this.labelFacebookEmail.AutoSize = true;
            this.labelFacebookEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelFacebookEmail.Location = new System.Drawing.Point(13, 162);
            this.labelFacebookEmail.Name = "labelFacebookEmail";
            this.labelFacebookEmail.Size = new System.Drawing.Size(133, 16);
            this.labelFacebookEmail.TabIndex = 16;
            this.labelFacebookEmail.Text = "Facebook e-mail: ";
            // 
            // labelPasswordUsername
            // 
            this.labelPasswordUsername.AutoSize = true;
            this.labelPasswordUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelPasswordUsername.Location = new System.Drawing.Point(13, 134);
            this.labelPasswordUsername.Name = "labelPasswordUsername";
            this.labelPasswordUsername.Size = new System.Drawing.Size(48, 16);
            this.labelPasswordUsername.TabIndex = 15;
            this.labelPasswordUsername.Text = "Sifra: ";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelUserName.Location = new System.Drawing.Point(13, 106);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(117, 16);
            this.labelUserName.TabIndex = 14;
            this.labelUserName.Text = "Korisnicko ime: ";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelSurname.Location = new System.Drawing.Point(13, 78);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(72, 16);
            this.labelSurname.TabIndex = 13;
            this.labelSurname.Text = "Prezime: ";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelName.Location = new System.Drawing.Point(13, 50);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(41, 16);
            this.labelName.TabIndex = 12;
            this.labelName.Text = "Ime: ";
            // 
            // textBoxNameUser
            // 
            this.textBoxNameUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxNameUser.Location = new System.Drawing.Point(202, 47);
            this.textBoxNameUser.Name = "textBoxNameUser";
            this.textBoxNameUser.Size = new System.Drawing.Size(285, 22);
            this.textBoxNameUser.TabIndex = 1;
            // 
            // textBoxSurnameUser
            // 
            this.textBoxSurnameUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxSurnameUser.Location = new System.Drawing.Point(202, 75);
            this.textBoxSurnameUser.Name = "textBoxSurnameUser";
            this.textBoxSurnameUser.Size = new System.Drawing.Size(285, 22);
            this.textBoxSurnameUser.TabIndex = 2;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxUserName.Location = new System.Drawing.Point(202, 103);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(285, 22);
            this.textBoxUserName.TabIndex = 3;
            // 
            // dataGridViewUser
            // 
            this.dataGridViewUser.AllowUserToOrderColumns = true;
            this.dataGridViewUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUser.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.intIdJela,
            this.txtNazivJela,
            this.txtKolicina,
            this.txtJedinicaMere,
            this.intPrice,
            this.txtValuta,
            this.cmbKategorija});
            this.dataGridViewUser.Location = new System.Drawing.Point(1, 485);
            this.dataGridViewUser.Name = "dataGridViewUser";
            this.dataGridViewUser.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewUser.Size = new System.Drawing.Size(1170, 251);
            this.dataGridViewUser.TabIndex = 0;
            // 
            // intIdJela
            // 
            this.intIdJela.DataPropertyName = "IdFood";
            this.intIdJela.FillWeight = 75.00001F;
            this.intIdJela.HeaderText = "IdJela";
            this.intIdJela.Name = "intIdJela";
            // 
            // txtNazivJela
            // 
            this.txtNazivJela.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtNazivJela.DataPropertyName = "FoodName";
            this.txtNazivJela.FillWeight = 83.33333F;
            this.txtNazivJela.HeaderText = "NazivJela";
            this.txtNazivJela.Name = "txtNazivJela";
            // 
            // txtKolicina
            // 
            this.txtKolicina.DataPropertyName = "Quantity";
            this.txtKolicina.FillWeight = 41.66666F;
            this.txtKolicina.HeaderText = "Kolicina";
            this.txtKolicina.Name = "txtKolicina";
            this.txtKolicina.Width = 150;
            // 
            // txtJedinicaMere
            // 
            this.txtJedinicaMere.DataPropertyName = "UnitOfMeasure";
            this.txtJedinicaMere.HeaderText = "JedinicaMere";
            this.txtJedinicaMere.Name = "txtJedinicaMere";
            this.txtJedinicaMere.Width = 150;
            // 
            // intPrice
            // 
            this.intPrice.DataPropertyName = "Price";
            this.intPrice.FillWeight = 25F;
            this.intPrice.HeaderText = "Cena";
            this.intPrice.Name = "intPrice";
            this.intPrice.Width = 150;
            // 
            // txtValuta
            // 
            this.txtValuta.DataPropertyName = "Currency";
            this.txtValuta.FillWeight = 20F;
            this.txtValuta.HeaderText = "Valuta";
            this.txtValuta.Name = "txtValuta";
            // 
            // cmbKategorija
            // 
            this.cmbKategorija.DataPropertyName = "CategoryFood";
            this.cmbKategorija.HeaderText = "Kategorija";
            this.cmbKategorija.Name = "cmbKategorija";
            this.cmbKategorija.Width = 150;
            // 
            // textBoxPasswordUserName
            // 
            this.textBoxPasswordUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxPasswordUserName.Location = new System.Drawing.Point(202, 131);
            this.textBoxPasswordUserName.Name = "textBoxPasswordUserName";
            this.textBoxPasswordUserName.Size = new System.Drawing.Size(285, 22);
            this.textBoxPasswordUserName.TabIndex = 4;
            // 
            // pictureBoxDrink
            // 
            this.pictureBoxDrink.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDrink.BackgroundImage")));
            this.pictureBoxDrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDrink.Location = new System.Drawing.Point(667, 97);
            this.pictureBoxDrink.Name = "pictureBoxDrink";
            this.pictureBoxDrink.Size = new System.Drawing.Size(367, 259);
            this.pictureBoxDrink.TabIndex = 11;
            this.pictureBoxDrink.TabStop = false;
            // 
            // buttonEditUser
            // 
            this.buttonEditUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditUser.Location = new System.Drawing.Point(400, 420);
            this.buttonEditUser.Name = "buttonEditUser";
            this.buttonEditUser.Size = new System.Drawing.Size(87, 36);
            this.buttonEditUser.TabIndex = 8;
            this.buttonEditUser.Text = "Izmeni";
            this.buttonEditUser.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteUser
            // 
            this.buttonDeleteUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteUser.Location = new System.Drawing.Point(304, 420);
            this.buttonDeleteUser.Name = "buttonDeleteUser";
            this.buttonDeleteUser.Size = new System.Drawing.Size(90, 36);
            this.buttonDeleteUser.TabIndex = 7;
            this.buttonDeleteUser.Text = "Obrisi";
            this.buttonDeleteUser.UseVisualStyleBackColor = true;
            // 
            // buttonAddUser
            // 
            this.buttonAddUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddUser.Location = new System.Drawing.Point(202, 420);
            this.buttonAddUser.Name = "buttonAddUser";
            this.buttonAddUser.Size = new System.Drawing.Size(88, 36);
            this.buttonAddUser.TabIndex = 6;
            this.buttonAddUser.Text = "Dodaj";
            this.buttonAddUser.UseVisualStyleBackColor = true;
            // 
            // textBoxFacebookEmail
            // 
            this.textBoxFacebookEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxFacebookEmail.Location = new System.Drawing.Point(202, 159);
            this.textBoxFacebookEmail.Name = "textBoxFacebookEmail";
            this.textBoxFacebookEmail.Size = new System.Drawing.Size(285, 22);
            this.textBoxFacebookEmail.TabIndex = 5;
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(1326, 2);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(38, 28);
            this.buttonClose.TabIndex = 33;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1290, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 28);
            this.button1.TabIndex = 34;
            this.button1.Text = "_";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Users
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panelDrinks);
            this.Controls.Add(this.panelMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Users";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Users";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Users_Load);
            this.panelMenu.ResumeLayout(false);
            this.panelDrinks.ResumeLayout(false);
            this.panelDrinks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.Button buttonUsers;
        private System.Windows.Forms.Button buttonOrderItems;
        private System.Windows.Forms.Button buttonOrders;
        private System.Windows.Forms.Button buttonFoods;
        private System.Windows.Forms.Button buttonDrinks;
        private System.Windows.Forms.Panel panelDrinks;
        private System.Windows.Forms.Label labelPasswordFacebook;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelIdUser;
        private System.Windows.Forms.TextBox textBoxIdUser;
        private System.Windows.Forms.Label labelFacebookEmail;
        private System.Windows.Forms.Label labelPasswordUsername;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxNameUser;
        private System.Windows.Forms.TextBox textBoxSurnameUser;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.DataGridView dataGridViewUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn intIdJela;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNazivJela;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKolicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtJedinicaMere;
        private System.Windows.Forms.DataGridViewTextBoxColumn intPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtValuta;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmbKategorija;
        private System.Windows.Forms.TextBox textBoxPasswordUserName;
        private System.Windows.Forms.PictureBox pictureBoxDrink;
        private System.Windows.Forms.Button buttonEditUser;
        private System.Windows.Forms.Button buttonDeleteUser;
        private System.Windows.Forms.Button buttonAddUser;
        private System.Windows.Forms.TextBox textBoxFacebookEmail;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxPasswordInstagram;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelUsernameInstagram;
        private System.Windows.Forms.TextBox textBoxUserNameInstagram;
        private System.Windows.Forms.TextBox textBoxPasswordEmail;
        private System.Windows.Forms.Label labelPasswordEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox textBoxFacebookPassword;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelFavoriteFood;
        private System.Windows.Forms.TextBox textBoxFavoriteFood;
        private System.Windows.Forms.TextBox textBoxFavoriteCity;
        private System.Windows.Forms.Label labelFavoriteCity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelNumberOfShoes;
        private System.Windows.Forms.TextBox textBoxNumberOfShoes;
        private System.Windows.Forms.CheckBox checkBoxIsAdmin;
    }
}