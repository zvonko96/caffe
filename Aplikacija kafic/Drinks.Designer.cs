﻿namespace Aplikacija_kafic
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonUsers = new System.Windows.Forms.Button();
            this.buttonOrderItems = new System.Windows.Forms.Button();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonFoods = new System.Windows.Forms.Button();
            this.buttonDrinks = new System.Windows.Forms.Button();
            this.panelDrinks = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxCategoryDrink = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelIdDrink = new System.Windows.Forms.Label();
            this.textBoxIdDrink = new System.Windows.Forms.TextBox();
            this.labelCurrencyDrink = new System.Windows.Forms.Label();
            this.labelPriceDrink = new System.Windows.Forms.Label();
            this.labelUnitOfMeasureDrink = new System.Windows.Forms.Label();
            this.labelQuantityDrink = new System.Windows.Forms.Label();
            this.labelDrinkName = new System.Windows.Forms.Label();
            this.textBoxDrinkName = new System.Windows.Forms.TextBox();
            this.textBoxQuantityDrink = new System.Windows.Forms.TextBox();
            this.textBoxUnitOfMeasureDrink = new System.Windows.Forms.TextBox();
            this.dataGridViewDrink = new System.Windows.Forms.DataGridView();
            this.intIdPica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNazivPica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtJedinicaMere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.intPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValuta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbKategorija = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxPriceDrink = new System.Windows.Forms.TextBox();
            this.pictureBoxDrink = new System.Windows.Forms.PictureBox();
            this.buttonEditDrink = new System.Windows.Forms.Button();
            this.buttonDeleteDrink = new System.Windows.Forms.Button();
            this.buttonAddDrink = new System.Windows.Forms.Button();
            this.textBoxCurrencyDrink = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panelMenu.SuspendLayout();
            this.panelDrinks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDrink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelMenu.Controls.Add(this.button2);
            this.panelMenu.Controls.Add(this.buttonUsers);
            this.panelMenu.Controls.Add(this.buttonOrderItems);
            this.panelMenu.Controls.Add(this.buttonOrders);
            this.panelMenu.Controls.Add(this.buttonFoods);
            this.panelMenu.Controls.Add(this.buttonDrinks);
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(192, 772);
            this.panelMenu.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(5, 705);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(180, 52);
            this.button2.TabIndex = 7;
            this.button2.Text = "Log out";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // buttonUsers
            // 
            this.buttonUsers.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUsers.Location = new System.Drawing.Point(5, 211);
            this.buttonUsers.Name = "buttonUsers";
            this.buttonUsers.Size = new System.Drawing.Size(180, 52);
            this.buttonUsers.TabIndex = 4;
            this.buttonUsers.Text = "Korisnici";
            this.buttonUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonUsers.UseVisualStyleBackColor = false;
            this.buttonUsers.Click += new System.EventHandler(this.buttonUsers_Click);
            // 
            // buttonOrderItems
            // 
            this.buttonOrderItems.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrderItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrderItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrderItems.Location = new System.Drawing.Point(5, 159);
            this.buttonOrderItems.Name = "buttonOrderItems";
            this.buttonOrderItems.Size = new System.Drawing.Size(180, 52);
            this.buttonOrderItems.TabIndex = 3;
            this.buttonOrderItems.Text = "Stavke narudzbine";
            this.buttonOrderItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOrderItems.UseVisualStyleBackColor = false;
            this.buttonOrderItems.Click += new System.EventHandler(this.buttonOrderItems_Click);
            // 
            // buttonOrders
            // 
            this.buttonOrders.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrders.Location = new System.Drawing.Point(5, 107);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Size = new System.Drawing.Size(180, 52);
            this.buttonOrders.TabIndex = 2;
            this.buttonOrders.Text = "Narudzbine";
            this.buttonOrders.UseVisualStyleBackColor = false;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonFoods
            // 
            this.buttonFoods.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonFoods.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFoods.Location = new System.Drawing.Point(5, 55);
            this.buttonFoods.Name = "buttonFoods";
            this.buttonFoods.Size = new System.Drawing.Size(180, 52);
            this.buttonFoods.TabIndex = 1;
            this.buttonFoods.Text = "Jela";
            this.buttonFoods.UseVisualStyleBackColor = false;
            this.buttonFoods.Click += new System.EventHandler(this.buttonFoods_Click);
            // 
            // buttonDrinks
            // 
            this.buttonDrinks.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonDrinks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDrinks.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonDrinks.Location = new System.Drawing.Point(5, 3);
            this.buttonDrinks.Name = "buttonDrinks";
            this.buttonDrinks.Size = new System.Drawing.Size(180, 52);
            this.buttonDrinks.TabIndex = 0;
            this.buttonDrinks.Text = "Pica";
            this.buttonDrinks.UseVisualStyleBackColor = false;
            this.buttonDrinks.Click += new System.EventHandler(this.buttonDrinks_Click);
            // 
            // panelDrinks
            // 
            this.panelDrinks.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelDrinks.Controls.Add(this.label7);
            this.panelDrinks.Controls.Add(this.label5);
            this.panelDrinks.Controls.Add(this.comboBoxCategoryDrink);
            this.panelDrinks.Controls.Add(this.label6);
            this.panelDrinks.Controls.Add(this.label4);
            this.panelDrinks.Controls.Add(this.label3);
            this.panelDrinks.Controls.Add(this.label2);
            this.panelDrinks.Controls.Add(this.label1);
            this.panelDrinks.Controls.Add(this.labelIdDrink);
            this.panelDrinks.Controls.Add(this.textBoxIdDrink);
            this.panelDrinks.Controls.Add(this.labelCurrencyDrink);
            this.panelDrinks.Controls.Add(this.labelPriceDrink);
            this.panelDrinks.Controls.Add(this.labelUnitOfMeasureDrink);
            this.panelDrinks.Controls.Add(this.labelQuantityDrink);
            this.panelDrinks.Controls.Add(this.labelDrinkName);
            this.panelDrinks.Controls.Add(this.textBoxDrinkName);
            this.panelDrinks.Controls.Add(this.textBoxQuantityDrink);
            this.panelDrinks.Controls.Add(this.textBoxUnitOfMeasureDrink);
            this.panelDrinks.Controls.Add(this.dataGridViewDrink);
            this.panelDrinks.Controls.Add(this.textBoxPriceDrink);
            this.panelDrinks.Controls.Add(this.pictureBoxDrink);
            this.panelDrinks.Controls.Add(this.buttonEditDrink);
            this.panelDrinks.Controls.Add(this.buttonDeleteDrink);
            this.panelDrinks.Controls.Add(this.buttonAddDrink);
            this.panelDrinks.Controls.Add(this.textBoxCurrencyDrink);
            this.panelDrinks.Location = new System.Drawing.Point(193, 36);
            this.panelDrinks.Name = "panelDrinks";
            this.panelDrinks.Size = new System.Drawing.Size(1173, 731);
            this.panelDrinks.TabIndex = 1;
            this.panelDrinks.Paint += new System.Windows.Forms.PaintEventHandler(this.panelDrinks_Paint);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(73, 370);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 18);
            this.label7.TabIndex = 32;
            this.label7.Text = "Kategorija: ";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(493, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 26);
            this.label5.TabIndex = 31;
            this.label5.Text = "*";
            // 
            // comboBoxCategoryDrink
            // 
            this.comboBoxCategoryDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.comboBoxCategoryDrink.FormattingEnabled = true;
            this.comboBoxCategoryDrink.Items.AddRange(new object[] {
            "Pivo",
            "Rakija",
            "Vino",
            "Gazirani sokovi",
            "Negazirani sokovi",
            "Viski"});
            this.comboBoxCategoryDrink.Location = new System.Drawing.Point(202, 367);
            this.comboBoxCategoryDrink.Name = "comboBoxCategoryDrink";
            this.comboBoxCategoryDrink.Size = new System.Drawing.Size(285, 26);
            this.comboBoxCategoryDrink.TabIndex = 30;
            this.comboBoxCategoryDrink.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategoryDrink_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(493, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 26);
            this.label6.TabIndex = 29;
            this.label6.Text = "*";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(493, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 26);
            this.label4.TabIndex = 28;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(493, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 26);
            this.label3.TabIndex = 27;
            this.label3.Text = "*";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(493, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 26);
            this.label2.TabIndex = 26;
            this.label2.Text = "*";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(493, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 26);
            this.label1.TabIndex = 25;
            this.label1.Text = "*";
            // 
            // labelIdDrink
            // 
            this.labelIdDrink.AutoSize = true;
            this.labelIdDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdDrink.Location = new System.Drawing.Point(73, 74);
            this.labelIdDrink.Name = "labelIdDrink";
            this.labelIdDrink.Size = new System.Drawing.Size(67, 18);
            this.labelIdDrink.TabIndex = 18;
            this.labelIdDrink.Text = "Id pica: ";
            this.labelIdDrink.Visible = false;
            // 
            // textBoxIdDrink
            // 
            this.textBoxIdDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBoxIdDrink.Location = new System.Drawing.Point(202, 71);
            this.textBoxIdDrink.Name = "textBoxIdDrink";
            this.textBoxIdDrink.Size = new System.Drawing.Size(285, 24);
            this.textBoxIdDrink.TabIndex = 17;
            this.textBoxIdDrink.Visible = false;
            // 
            // labelCurrencyDrink
            // 
            this.labelCurrencyDrink.AutoSize = true;
            this.labelCurrencyDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrencyDrink.Location = new System.Drawing.Point(73, 324);
            this.labelCurrencyDrink.Name = "labelCurrencyDrink";
            this.labelCurrencyDrink.Size = new System.Drawing.Size(64, 18);
            this.labelCurrencyDrink.TabIndex = 16;
            this.labelCurrencyDrink.Text = "Valuta: ";
            // 
            // labelPriceDrink
            // 
            this.labelPriceDrink.AutoSize = true;
            this.labelPriceDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPriceDrink.Location = new System.Drawing.Point(73, 271);
            this.labelPriceDrink.Name = "labelPriceDrink";
            this.labelPriceDrink.Size = new System.Drawing.Size(57, 18);
            this.labelPriceDrink.TabIndex = 15;
            this.labelPriceDrink.Text = "Cena: ";
            // 
            // labelUnitOfMeasureDrink
            // 
            this.labelUnitOfMeasureDrink.AutoSize = true;
            this.labelUnitOfMeasureDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnitOfMeasureDrink.Location = new System.Drawing.Point(73, 226);
            this.labelUnitOfMeasureDrink.Name = "labelUnitOfMeasureDrink";
            this.labelUnitOfMeasureDrink.Size = new System.Drawing.Size(123, 18);
            this.labelUnitOfMeasureDrink.TabIndex = 14;
            this.labelUnitOfMeasureDrink.Text = "Jedinica mere: ";
            // 
            // labelQuantityDrink
            // 
            this.labelQuantityDrink.AutoSize = true;
            this.labelQuantityDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuantityDrink.Location = new System.Drawing.Point(73, 174);
            this.labelQuantityDrink.Name = "labelQuantityDrink";
            this.labelQuantityDrink.Size = new System.Drawing.Size(78, 18);
            this.labelQuantityDrink.TabIndex = 13;
            this.labelQuantityDrink.Text = "Kolicina: ";
            // 
            // labelDrinkName
            // 
            this.labelDrinkName.AutoSize = true;
            this.labelDrinkName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDrinkName.Location = new System.Drawing.Point(73, 122);
            this.labelDrinkName.Name = "labelDrinkName";
            this.labelDrinkName.Size = new System.Drawing.Size(96, 18);
            this.labelDrinkName.TabIndex = 12;
            this.labelDrinkName.Text = "Naziv pica: ";
            // 
            // textBoxDrinkName
            // 
            this.textBoxDrinkName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDrinkName.Location = new System.Drawing.Point(202, 119);
            this.textBoxDrinkName.Name = "textBoxDrinkName";
            this.textBoxDrinkName.Size = new System.Drawing.Size(285, 24);
            this.textBoxDrinkName.TabIndex = 1;
            this.textBoxDrinkName.TextChanged += new System.EventHandler(this.textBoxDrinkName_TextChanged);
            // 
            // textBoxQuantityDrink
            // 
            this.textBoxQuantityDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQuantityDrink.Location = new System.Drawing.Point(202, 171);
            this.textBoxQuantityDrink.Name = "textBoxQuantityDrink";
            this.textBoxQuantityDrink.Size = new System.Drawing.Size(285, 24);
            this.textBoxQuantityDrink.TabIndex = 2;
            this.textBoxQuantityDrink.TextChanged += new System.EventHandler(this.textBoxQuantityDrink_TextChanged);
            // 
            // textBoxUnitOfMeasureDrink
            // 
            this.textBoxUnitOfMeasureDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUnitOfMeasureDrink.Location = new System.Drawing.Point(202, 223);
            this.textBoxUnitOfMeasureDrink.Name = "textBoxUnitOfMeasureDrink";
            this.textBoxUnitOfMeasureDrink.Size = new System.Drawing.Size(285, 24);
            this.textBoxUnitOfMeasureDrink.TabIndex = 3;
            this.textBoxUnitOfMeasureDrink.TextChanged += new System.EventHandler(this.textBoxUnitOfMeasureDrink_TextChanged);
            // 
            // dataGridViewDrink
            // 
            this.dataGridViewDrink.AllowUserToOrderColumns = true;
            this.dataGridViewDrink.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDrink.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.intIdPica,
            this.txtNazivPica,
            this.txtKolicina,
            this.txtJedinicaMere,
            this.intPrice,
            this.txtValuta,
            this.cmbKategorija});
            this.dataGridViewDrink.Location = new System.Drawing.Point(1, 485);
            this.dataGridViewDrink.Name = "dataGridViewDrink";
            this.dataGridViewDrink.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewDrink.Size = new System.Drawing.Size(1170, 251);
            this.dataGridViewDrink.TabIndex = 0;
            this.dataGridViewDrink.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDrink_CellContentClick);
            this.dataGridViewDrink.Click += new System.EventHandler(this.dataGridViewDrink_Click);
            // 
            // intIdPica
            // 
            this.intIdPica.DataPropertyName = "IdDrink";
            this.intIdPica.FillWeight = 75.00001F;
            this.intIdPica.HeaderText = "IdPica";
            this.intIdPica.Name = "intIdPica";
            // 
            // txtNazivPica
            // 
            this.txtNazivPica.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtNazivPica.DataPropertyName = "DrinkName";
            this.txtNazivPica.FillWeight = 83.33333F;
            this.txtNazivPica.HeaderText = "NazivPica";
            this.txtNazivPica.Name = "txtNazivPica";
            // 
            // txtKolicina
            // 
            this.txtKolicina.DataPropertyName = "Quantity";
            this.txtKolicina.FillWeight = 41.66666F;
            this.txtKolicina.HeaderText = "Kolicina";
            this.txtKolicina.Name = "txtKolicina";
            this.txtKolicina.Width = 150;
            // 
            // txtJedinicaMere
            // 
            this.txtJedinicaMere.DataPropertyName = "UnitOfMeasure";
            this.txtJedinicaMere.HeaderText = "JedinicaMere";
            this.txtJedinicaMere.Name = "txtJedinicaMere";
            this.txtJedinicaMere.Width = 150;
            // 
            // intPrice
            // 
            this.intPrice.DataPropertyName = "Price";
            this.intPrice.FillWeight = 25F;
            this.intPrice.HeaderText = "Cena";
            this.intPrice.Name = "intPrice";
            this.intPrice.Width = 150;
            // 
            // txtValuta
            // 
            this.txtValuta.DataPropertyName = "Currency";
            this.txtValuta.FillWeight = 20F;
            this.txtValuta.HeaderText = "Valuta";
            this.txtValuta.Name = "txtValuta";
            // 
            // cmbKategorija
            // 
            this.cmbKategorija.DataPropertyName = "CategoryDrink";
            this.cmbKategorija.HeaderText = "Kategorija";
            this.cmbKategorija.Name = "cmbKategorija";
            this.cmbKategorija.Width = 150;
            // 
            // textBoxPriceDrink
            // 
            this.textBoxPriceDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPriceDrink.Location = new System.Drawing.Point(202, 268);
            this.textBoxPriceDrink.Name = "textBoxPriceDrink";
            this.textBoxPriceDrink.Size = new System.Drawing.Size(285, 24);
            this.textBoxPriceDrink.TabIndex = 4;
            this.textBoxPriceDrink.TextChanged += new System.EventHandler(this.textBoxPriceDrink_TextChanged);
            // 
            // pictureBoxDrink
            // 
            this.pictureBoxDrink.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDrink.BackgroundImage")));
            this.pictureBoxDrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDrink.Location = new System.Drawing.Point(537, 108);
            this.pictureBoxDrink.Name = "pictureBoxDrink";
            this.pictureBoxDrink.Size = new System.Drawing.Size(587, 196);
            this.pictureBoxDrink.TabIndex = 11;
            this.pictureBoxDrink.TabStop = false;
            // 
            // buttonEditDrink
            // 
            this.buttonEditDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditDrink.Location = new System.Drawing.Point(400, 412);
            this.buttonEditDrink.Name = "buttonEditDrink";
            this.buttonEditDrink.Size = new System.Drawing.Size(87, 36);
            this.buttonEditDrink.TabIndex = 8;
            this.buttonEditDrink.Text = "Izmeni";
            this.buttonEditDrink.UseVisualStyleBackColor = true;
            this.buttonEditDrink.Click += new System.EventHandler(this.buttonEditDrink_Click);
            // 
            // buttonDeleteDrink
            // 
            this.buttonDeleteDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteDrink.Location = new System.Drawing.Point(304, 412);
            this.buttonDeleteDrink.Name = "buttonDeleteDrink";
            this.buttonDeleteDrink.Size = new System.Drawing.Size(90, 36);
            this.buttonDeleteDrink.TabIndex = 7;
            this.buttonDeleteDrink.Text = "Obrisi";
            this.buttonDeleteDrink.UseVisualStyleBackColor = true;
            this.buttonDeleteDrink.Click += new System.EventHandler(this.buttonDeleteDrink_Click);
            // 
            // buttonAddDrink
            // 
            this.buttonAddDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddDrink.Location = new System.Drawing.Point(202, 412);
            this.buttonAddDrink.Name = "buttonAddDrink";
            this.buttonAddDrink.Size = new System.Drawing.Size(88, 36);
            this.buttonAddDrink.TabIndex = 6;
            this.buttonAddDrink.Text = "Dodaj";
            this.buttonAddDrink.UseVisualStyleBackColor = true;
            this.buttonAddDrink.Click += new System.EventHandler(this.buttonAddDrink_Click);
            // 
            // textBoxCurrencyDrink
            // 
            this.textBoxCurrencyDrink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCurrencyDrink.Location = new System.Drawing.Point(202, 321);
            this.textBoxCurrencyDrink.Name = "textBoxCurrencyDrink";
            this.textBoxCurrencyDrink.Size = new System.Drawing.Size(285, 24);
            this.textBoxCurrencyDrink.TabIndex = 5;
            this.textBoxCurrencyDrink.TextChanged += new System.EventHandler(this.textBoxCurrencyDrink_TextChanged);
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(1326, 2);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(38, 28);
            this.buttonClose.TabIndex = 5;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1290, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 28);
            this.button1.TabIndex = 6;
            this.button1.Text = "_";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.panelDrinks);
            this.Controls.Add(this.panelMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Settings_Load);
            this.panelMenu.ResumeLayout(false);
            this.panelDrinks.ResumeLayout(false);
            this.panelDrinks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDrink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrink)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonDrinks;
        private System.Windows.Forms.Button buttonFoods;
        private System.Windows.Forms.Button buttonOrderItems;
        private System.Windows.Forms.Button buttonOrders;
        private System.Windows.Forms.Button buttonUsers;
        private System.Windows.Forms.Panel panelDrinks;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridViewDrink;
        private System.Windows.Forms.TextBox textBoxCurrencyDrink;
        private System.Windows.Forms.Button buttonEditDrink;
        private System.Windows.Forms.Button buttonDeleteDrink;
        private System.Windows.Forms.Button buttonAddDrink;
        private System.Windows.Forms.PictureBox pictureBoxDrink;
        private System.Windows.Forms.TextBox textBoxDrinkName;
        private System.Windows.Forms.TextBox textBoxQuantityDrink;
        private System.Windows.Forms.TextBox textBoxUnitOfMeasureDrink;
        private System.Windows.Forms.TextBox textBoxPriceDrink;
        private System.Windows.Forms.Label labelCurrencyDrink;
        private System.Windows.Forms.Label labelPriceDrink;
        private System.Windows.Forms.Label labelUnitOfMeasureDrink;
        private System.Windows.Forms.Label labelQuantityDrink;
        private System.Windows.Forms.Label labelDrinkName;
        private System.Windows.Forms.Label labelIdDrink;
        private System.Windows.Forms.TextBox textBoxIdDrink;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxCategoryDrink;
        private System.Windows.Forms.DataGridViewTextBoxColumn intIdPica;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNazivPica;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKolicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtJedinicaMere;
        private System.Windows.Forms.DataGridViewTextBoxColumn intPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtValuta;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmbKategorija;
    }
}