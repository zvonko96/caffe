﻿using DL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Aplikacija_kafic
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
      

        private void buttonLoginAdminExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonLoginAdminMinimized_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonLoginAdminExit_MouseHover(object sender, EventArgs e)
        {
            this.buttonLoginAdminExit.BackColor = Color.Maroon;
        }

        private void buttonLoginAdminExit_MouseLeave(object sender, EventArgs e)
        {
            this.buttonLoginAdminExit.BackColor = Color.DarkSlateGray;
        }




        private void button4_Click(object sender, EventArgs e)
        {
        }
        private void LoginAdmin_Load(object sender, EventArgs e)
        {
            timerLoginAdmin.Start();
            labelLoginAdminTime.Text = DateTime.Now.ToLongTimeString();
            label1.Hide();
            linkLabelForgotLoginAdmin.Hide();
        }

       

        private void timerLoginAdmin_Tick_1(object sender, EventArgs e)
        {
            labelLoginAdminTime.Text = DateTime.Now.ToLongTimeString();
            timerLoginAdmin.Start();
        }

        private void buttonLoginAdmin_Click(object sender, EventArgs e)
        {
            if (textBoxLoginAdminUserName.Text != "" && textBoxLoginAdminPassword.Text != "")
            {
                SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                SqlDataAdapter sda = new SqlDataAdapter("select isAdmin from [User] where UserName='" + textBoxLoginAdminUserName.Text + "' and PasswordUserName='" + textBoxLoginAdminPassword.Text + "'", conn);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                if (dt.Rows.Count == 1)
                {

                    if (dt.Rows[0][0].ToString() == "True")
                    {
                        this.Hide();
                        Settings mm = new Settings();
                        mm.Show();
                    }
                    else
                    {
                        this.Hide();
                        Waiter wt = new Waiter();
                        wt.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Pogresno ste uneli korisnicko ime ili lozinku!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    label1.Show();
                    linkLabelForgotLoginAdmin.Show();
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxLoginAdminUserName_TextChanged(object sender, EventArgs e)
        {
            if(textBoxLoginAdminUserName.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide() ;
            }
        }

        private void textBoxLoginAdminPassword_TextChanged(object sender, EventArgs e)
        {
            if (textBoxLoginAdminPassword.Text == "")
            {
                label6.Show() ;
            }
            else
            {
                label6.Hide();
            }
        }

       

       
        bool durum ;
        private void buttonFacebookAdmin_Click(object sender, EventArgs e)
        {
            
            if (durum != true)
            {
                facebookLogin fb = new facebookLogin();
                fb.Show();
                durum = true;
            }
            else
            {
                durum = false;
                facebookLogin fb = new facebookLogin();
                fb.Hide();

            }
           

        }

        private void buttonInstagramAdmin_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                instagramLogin ig = new instagramLogin();
                ig.Show();
                durum = true;
            }
            else
            {
                durum = false;
                instagramLogin ig = new instagramLogin();
                ig.Hide();

            }
        }

        private void buttonGmailAdmin_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                googleLogin go = new googleLogin();
                go.Show();
                durum = true;
            }
            else
            {
                durum = false;
                googleLogin go = new googleLogin();
                go.Hide();

            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxLoginAdminPassword.UseSystemPasswordChar = false;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxLoginAdminPassword.UseSystemPasswordChar = true;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void linkLabelForgotLoginAdmin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RecoverPassword rv = new RecoverPassword();
            rv.Show();
        }
    }
}
