﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija_kafic
{
    public partial class facebookLogin : Form
    {
        public facebookLogin()
        {
            InitializeComponent();
        }

        private void buttonLoginFacebook_Click(object sender, EventArgs e)
        {
            if (textBoxEmailFbLogin.Text != "" && textBoxPasswordFbLogin.Text != "")
            {
                SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Kafic;Integrated Security=True;Connect Timeout=300;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                SqlDataAdapter sda = new SqlDataAdapter("select isAdmin from [User] where FacebookEmail='" + textBoxEmailFbLogin.Text + "' and PasswordFacebook='" + textBoxPasswordFbLogin.Text + "'", conn);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                if (dt.Rows.Count == 1)
                {

                    if (dt.Rows[0][0].ToString() == "True")
                    {
                        this.Hide();
                        Settings mm = new Settings();
                        mm.Show();
                    }
                    else
                    {
                        this.Hide();
                        Waiter wt = new Waiter();
                        wt.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Pogresno ste uneli email ili lozinku!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja koja su oznacena zvezdicom!", "Upozorenje!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                label1.Show();
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        bool durum;
        private void pictureBoxEye_Click(object sender, EventArgs e)
        {
            if (durum != true)
            {
                textBoxPasswordFbLogin.UseSystemPasswordChar = false;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce1;
                durum = true;
            }
            else
            {
                durum = false;
                textBoxPasswordFbLogin.UseSystemPasswordChar = true;
                pictureBoxEye.Image = Aplikacija_kafic.Properties.Resources.okce;
            }
        }

        private void textBoxEmailFbLogin_TextChanged(object sender, EventArgs e)
        {
            if (textBoxEmailFbLogin.Text == "")
            {
                label5.Show();
            }
            else
            {
                label5.Hide();
            }
        }

        private void textBoxPasswordFbLogin_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPasswordFbLogin.Text == "")
            {
                label2.Show();
            }
            else
            {
                label2.Hide();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
