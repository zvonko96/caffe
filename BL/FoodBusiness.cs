﻿using DL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class FoodBusiness
    {
        FoodRepository foodRepository;


        public FoodBusiness()
        {
            this.foodRepository = new FoodRepository();
        }

        public string NewFood(Food f)
        {
            int rows = this.foodRepository.InsertFood(f);
            if (rows > 0)
            {
                return "Uspešan unos u bazu podataka!";
            }
            else
            {
                return "Neuspešan unos!";
            }
        }

        public string DeleteFood(Food f)
        {
            int row = this.foodRepository.DeleteFood(f);
            if (row > 0)
            {
                return "Uspešan obrisan podatak iz baze!";
            }
            else
            {
                return "Neuspešno brisanje!";
            }
        }

        public string UpdateFood(Food f)
        {
            int row = this.foodRepository.UpdateFood(f);
            if (row > 0)
            {
                return "Uspešano su izmenjeni podaci u bazi!";
            }
            else
            {
                return "Neuspešno menjanje!";
            }
        }


        public List<Food> GetFoods()
        {
            return this.foodRepository.GetAllFoods().ToList();
        }
    }
}
