﻿using DL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
   public class DrinkBusiness
    {
        DrinkRepository drinkRepository;


        public DrinkBusiness()
        {
            this.drinkRepository = new DrinkRepository();
        }

        public string NewDrink(Drink d)
        {
            int rows = this.drinkRepository.InsertDrink(d);
            if (rows > 0)
            {
                return "Uspešan unos u bazu podataka!";
            }
            else
            {
                return "Neuspešan unos!";
            }
        }

        public string DeleteDrink(Drink d)
        {
            int row = this.drinkRepository.DeleteDrink(d);
            if (row > 0)
            {
                return "Uspešan obrisan podatak iz baze!";
            }
            else
            {
                return "Neuspešno brisanje!";
            }
        }

        public string UpdateDrink(Drink d)
        {
            int row = this.drinkRepository.UpdateDrink(d);
            if (row > 0)
            {
                return "Uspešano su izmenjeni podaci u bazi!";
            }
            else
            {
                return "Neuspešno menjanje!";
            }
        }


        public List<Drink> GetDrinks()
        {
            return this.drinkRepository.GetAllDrinks().ToList();
        }
    }
}
