﻿using DL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
  public  class OrdersBusiness
    {
        OrderRepository orderRepository;

        public OrdersBusiness()
        {
            this.orderRepository = new OrderRepository();
        }

        public string UpdateOrders(Orders o)
        {
            int row = this.orderRepository.UpdateOrder(o);
            if (row > 0)
            {
                return "Uspešano su izmenjeni podaci u bazi!";
            }
            else
            {
                return "Neuspešno menjanje!";
            }
        }

        public string NewOrder(Orders o)
        {
            int rows = this.orderRepository.InsertOrder(o);
            if (rows > 0)
            {
                return "Uspešan unos u bazu podataka!";
            }
            else
            {
                return "Neuspešan unos!";
            }
        }

        public string DeleteOrder(Orders o)
        {
            int row = this.orderRepository.DeleteOrderr(o);
            if (row > 0)
            {
                return "Uspešan obrisan podatak iz baze!";
            }
            else
            {
                return "Neuspešno brisanje!";
            }
        }





        public List<Orders> GetOrders()
        {
            return this.orderRepository.GetAllOrder().ToList();
        }
    }
}
