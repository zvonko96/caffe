﻿using DL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class UserBusiness
    {
        UserRepository userRepository;

        public UserBusiness()
        {
            this.userRepository = new UserRepository();
        }

        public string UpdatePassword(User u)
        {
            int row = this.userRepository.UpdatePassword(u);
            if (row > 0)
            {
                return "Uspešano su izmenjeni podaci u bazi!";
            }
            else
            {
                return "Neuspešno menjanje!";
            }
        }

        public string NewUser(User u)
        {
            int rows = this.userRepository.InsertUser(u);
            if (rows > 0)
            {
                return "Uspešan unos u bazu podataka!";
            }
            else
            {
                return "Neuspešan unos!";
            }
        }

        public string DeleteUser(User u)
        {
            int row = this.userRepository.DeleteUser(u);
            if (row > 0)
            {
                return "Uspešan obrisan podatak iz baze!";
            }
            else
            {
                return "Neuspešno brisanje!";
            }
        }

       
       


        public List<User> GetUsers()
        {
            return this.userRepository.GetAllUsers().ToList();
        }
    }
}
