﻿using DL;
using DL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
  public  class OrderItemsBusiness
    {
        OrderItemRepository orderItemRepository;

        public OrderItemsBusiness()
        {
            this.orderItemRepository = new OrderItemRepository();
        }

        public string UpdateOrderItems(OrderItem o)
        {
            int row = this.orderItemRepository.UpdateOrderItemr(o);
            if (row > 0)
            {
                return "Uspešano su izmenjeni podaci u bazi!";
            }
            else
            {
                return "Neuspešno menjanje!";
            }
        }

        public string NewOrderItem(OrderItem o)
        {
            int rows = this.orderItemRepository.InsertOrderItem(o);
            if (rows > 0)
            {
                return "Uspešan unos u bazu podataka!";
            }
            else
            {
                return "Neuspešan unos!";
            }
        }

        public string DeleteOrderItem(OrderItem o)
        {
            int row = this.orderItemRepository.DeleteOrderItem(o);
            if (row > 0)
            {
                return "Uspešan obrisan podatak iz baze!";
            }
            else
            {
                return "Neuspešno brisanje!";
            }
        }





        public List<OrderItem> GetOrders()
        {
            return this.orderItemRepository.GetAllOrderItem().ToList();
        }
    }
}
